<?php require_once "header.php"; ?>

<h1><span class="label label-default" id="span_full_name"></span></h1>
<a href="search.php" class="btn btn-default">&laquo; Back to Search</a>

<?php
if (isset($_GET['t'])) {
    $tkid = $_GET['t'];


    $db = Xcrud_db::get_instance();
    

    $select = "SELECT ph_lexref.tkinit,tklast,tkfirst from ph_lexref join timekeep on timekeep.tkinit=ph_lexref.tkinit where timekeep.tkinit =  '" . $_GET['t'] . "'";
    $db->query($select);
    $arr = $db->result();
    foreach ($arr as $r) {
        $tkid = $r['tkinit'];
        $tklast = $r['tklast'];
        $tkfirst = $r['tkfirst'];
    }
}

if (isset($_GET['l'])) {
    $lexis = $_GET['l'];

    // look tkid for this record
    $db = Xcrud_db::get_instance();
    $select = "SELECT ph_lexref.tkinit,tklast,tkfirst from ph_lexref join timekeep on timekeep.tkinit=ph_lexref.tkinit where userid =  '" . $_GET['l'] . "'";
    $db->query($select);
    $arr = $db->result();
    foreach ($arr as $r) {
        $tkid = $r['tkinit'];
        $tklast = $r['tklast'];
        $tkfirst = $r['tkfirst'];
    }
}

$xcrud = Xcrud::get_instance();
$xcrud->table('ph_lexref');

 $xcrud->before_insert('shownamecopy');
$xcrud->before_update('showname');
//$xcrud->pass_var('created_by','tkinit2', 'create');

if (isset($_GET['t'])) {
    $xcrud->where('tkinit', $tkid);
}
if (isset($_GET['l'])) {
    $xcrud->where('userid', $lexis);
}
$xcrud->set_attr('userid', array('id' => 'userid'));
$xcrud->set_attr('tkinit', array('id' => 'tkinit1'));
$xcrud->column_callback('tkinit', 'gettimekeeper1');
$xcrud->label('userid', 'Lexis ID');
$xcrud->label('tkinit', 'Timekeeper ID');
$xcrud->unset_search();
$xcrud->unset_print();
$xcrud->unset_csv();
$xcrud->unset_title();

// $xcrud->before_update('savetkinit','functions.php');

//$xcrud->before_update('', 'savetkinit', 'functions.php');


  
//       $xcrud->change_type('tkinit', 'select', '', xcrudDropDownOptions('lexiscrossref', 'tkinit', 'timekeeper', true));

$xcrud->set_attr('tkinit', array('id' => 'tkinit'));



echo $xcrud->render();
?>

<?php require_once "footer.php"; ?>

<script>

    jQuery(document).on("ready xcrudafterrequest", function () {

        $("#userid").keyup(function () {
            this.value = this.value.toLocaleUpperCase();
        });


        $("#span_full_name").text('');
        if (Xcrud.current_task == 'create') {
            $("#span_full_name").text($('#tkinit').val());
            $("#tkinit").val('<?php echo $tkid  . " - " . $tklast . ", " . $tkfirst  ?>');
            $("#tkinit").prop('readonly', true);
            
     



//$("#tkinit").prop('disabled', true);
//          $("#userid").val(<?php // echo $tkid      ?>)

        }
        
//                if (Xcrud.current_task == 'save')
//        {
//            $(".xcrud").hide();
//            window.location = "search.php";
//
//        }

        if (Xcrud.current_task == 'edit') {
//            alert();
            $("#tkinit").prop('readonly', true);

            $("#span_full_name").text($('#userid').val());
            $("#tkinit").val('<?php echo $tkid . " - " . $tklast . ", " . $tkfirst ?>');
             

        }
        
    });



    $("#tkinit1").prop('disabled', true);
</script>


