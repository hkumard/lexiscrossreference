<?php
$serverName = "phsrv60";
$connectionInfo = array("Database" => "Litsupport_TaskManager", "UID" => "dbiadm", "PWD" => "instAdm#li60s");

function strip_zeros_from_date( $marked_string="" ) {
  // first remove the marked zeros
  $no_zeros = str_replace('*0', '', $marked_string);
  // then remove any remaining marks
  $cleaned_string = str_replace('*', '', $no_zeros);
  return $cleaned_string;
}

function hide_spaces($marked_string="" ) {
  $cleaned_string = str_replace(' ', '-', $marked_string);
  return $cleaned_string;
}

function hide_spaces_tilde($marked_string="" ) {
  $cleaned_string = str_replace(' ', '~', $marked_string);
  return $cleaned_string;
}

function redirect_to( $location = NULL ) {
  if ($location != NULL) {
    header("Location: {$location}");
  //  exit;
  }
}

function output_message($message="") {
  if (!empty($message)) { 
    return "<p class=\"message\">{$message}</p>";
  } else {
    return "";
  }
}

/*function __autoload($class_name) {
	$class_name = strtolower($class_name);
  $path = LIB_PATH.DS."{$class_name}.php";
  if(file_exists($path)) {
    require_once($path);
  } else {
		die("The file {$class_name}.php could not be found.");
	}
}*/

function include_layout_template($template="") {
	include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}

function log_action($action, $message="") {
	$logfile = SITE_ROOT.DS.'logs'.DS.'log.txt';
	$new = file_exists($logfile) ? false : true;
  if($handle = fopen($logfile, 'a')) { // append
    $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
		$content = "{$timestamp} | {$action}: {$message}\n";
    fwrite($handle, $content);
    fclose($handle);
    if($new) { chmod($logfile, 0755); }
  } else {
    echo "Could not open log file for writing.";
  }
}

function datetime_to_text($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%A %B %d, %Y at %I:%M %p", $unixdatetime);
}

function get_current_quarter(){
    
    $month = date('n');
    
    if($month <=3) return "Q1".date('Y');
    if($month <=6) return "Q2".date('Y');
    if($month <=6) return "Q3".date('Y');
    
    return "Q4".date('Y');
}

function mssql_escape($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/',             // 14-31
            '/\xA0/',
            '/\xE2/',
            '/\x80/',
            '/\x8B/',
            '/\xAEA/',
            '/\x93/',
            '/\xE9/',
            '/\x99/',
            '/\xB7/',
            '/\xA6/',
            '/\x98/',
            '/\xAE/',
            '/\xBD/',
            '/\xC2/',
            '/\xA9/',
            '/\xC3/',
            '/\xA7/',
            '/\x9C/',
            '/\x9D/',
            '/\xC9/',
            '/\x0A/',
            '/\xBC/',
            '/\x84/',
            '/\xA2/',
            '/\xAD/',
            '/\x94/',
            '/\xF1/',
            '/\xB1/',
            '/\xB3/',
            '/\xBA/',
            '/\xA1/',
            '/\xB5/',
            '/\xE7/',
            '/\xED/',
            '/\xF3/'
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
    }

function full_day_date($value)
{
   $date = strtotime($value);
   $full_date= date('l m/d/Y',$date);
   return $full_date;
}

//XCRUD
function xcrudDropDownOptions($method,$val,$text,$blank)
{
    $json_array = getCURL($method);
    if ($blank) {
        $assoc_array[0]="";
    }
    foreach($json_array as $elem)
    {
        $assoc_array[$elem->$val] = $elem->$text;
    }
    return $assoc_array;
}

function sort_order_list($limit) {
    $ret = "'";
    for ($i=1; $i<=$limit; $i++){
        $ret .= $i . ',';
    }
    $ret = rtrim($ret, ',');
    $ret .= "'";
    return $ret;  
}

function getFirmUsers() {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT TimeKeeperID, DisplayName,Position FROM FirmDirectory WHERE Active = 1 ORDER BY DisplayName";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        foreach ($arr as $row) {
            $ret[$row["TimeKeeperID"]]=$row["DisplayName"];
        }
    }
    return $ret;
}

function getCommitteeRoles() {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT PKRoleID, RoleName FROM Role ORDER BY SortIndex DESC";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        foreach ($arr as $row) {
            $ret[$row["PKRoleID"]]=$row["RoleName"];
        }
    }
    return $ret;
}


?>
