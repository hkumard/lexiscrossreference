<?php

require_once("includes/config.php");
require_once("includes/functions.php");

/* GLOBAL */

function static_content($id) {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT content_id, content_text FROM static_content WHERE content_id = " . $id;
    $db->query($sql);
    $arr = $db->result();
    $output = '';
    foreach ($arr as $r) {

        if ($_SESSION['admin_visible']) {
            $output .= "<a href='admin/admin-static-content.php?c=" . $r["content_id"] . "' title='[ADMIN] Add/Edit Text' target='_blank' class='btn btn-default btn-xs btn-admin'><span class='glyphicon glyphicon glyphicon-edit'></span></a>";
        }
        $output .= "<div >" . $r['content_text'] . "</div>";
    }
    echo $output;
}

function document_library($id, $show_header = true, $max = 1000, $sort = 'sort_order,title') {
    $db = Xcrud_db::get_instance();
    $select = "SELECT doc_lib.dl_id, doc_lib_items.title, doc_lib_items.document, doc_lib_items.sort_order,
                doc_lib.folder_url, doc_lib.title AS doc_lib_title
                FROM doc_lib_items
                JOIN doc_lib ON doc_lib.dl_id = doc_lib_items.lib_id
                WHERE dl_id = " . $id . " AND visible = 1 ORDER BY $sort";
    $db->query($select);
    $arr = $db->result();
    $first = true;
    $output = "";
    $i = 0;
    foreach ($arr as $r) {
        $i++;
        if ($first) {
            $first = false;
            if ($show_header) {
                $output .= "<h2>" . $r["doc_lib_title"];
            }
            if ($_SESSION['admin_visible']) {
                $output .= "<a href='admin/admin-doc-lib-items.php?l=" . $r["dl_id"] . "' title='[ADMIN] Add/Edit/Remove Documents' target='_blank' class='btn btn-default btn-xs btn-admin' style='margin-bottom:-20px;'><span class='glyphicon glyphicon-folder-open'></span></a>";
            }
            if ($show_header) {
                $output .= "</h2>";
            }
            $output .= "<ul>";
        }
        $output .= '<li><a href="doclib/' . $r["folder_url"] . '/' . $r["document"] . '" target="_blank">' . $r["title"] . "</a></li>";
        if ($i == $max)
            break;
    }
    $output .= "</ul>";
    
    if ($output == "</ul>") {
        $select_title = "SELECT * FROM doc_lib WHERE dl_id = " . $id;
        $db->query($select_title);
        $arr_title = $db->result();
        if ($arr_title != null) {
            $output = "<h2>" . $arr_title[0]["title"];
            if ($_SESSION['admin_visible']) {
                $output .= "<a href='admin/admin-doc-lib-items.php?l=" . $arr_title[0]["dl_id"] . "' title='[ADMIN] Add/Edit/Remove Documents' target='_blank' class='btn btn-default btn-xs btn-admin' style='margin-bottom:-20px;'><span class='glyphicon glyphicon-folder-open'></span></a>";
            }
        }
        $output .= "</h2>";
    }
    echo $output;
}

function links_list($id, $title = "") {
    $db = Xcrud_db::get_instance();
    $select = "SELECT * FROM portal_list_items WHERE list_id = " . $id . " ORDER BY sort_order,text1";
    $db->query($select);
    $arr = $db->result();
    $first = true;
    $output = "";
    if ($title != "") {
        $output .= "<h2>" . $title . "</h2>";
    }
    if ($_SESSION['admin_visible']) {
        $output .= "<a href='admin/admin-portal-list-links.php?l=" . $id . "' title='[ADMIN] Add/Edit/Remove Links' target='_blank' class='btn btn-default btn-xs btn-admin' style='margin-bottom:-20px;'><span class='glyphicon glyphicon-th-list'></span></a>";
    }
    foreach ($arr as $r) {
        if ($first) {
            $first = false;

            $output .= "<ul>";
        }
        $output .= '<li><a href="' . $r["url1"] . '" >' . $r["text1"] . "</a></li>";
    }
    $output .= "</ul>";
    echo $output;
}

function news_headlines($class, $limit) {
    $db = Xcrud_db::get_instance();

    $select = "SELECT DISTINCT portal_news.* FROM portal_news
                JOIN portal_news_class_join ON (portal_news_class_join.news_id = portal_news.news_id)
                WHERE portal_news_class_join.class_id = " . $class .
            " AND (publish_date >= NOW() AND expire_date >= NOW()) ORDER BY publish_date DESC ";
    if ($limit > 0) {
        $select .= " LIMIT " . $limit . " ";
    }
    $db->query($select);
    $arr = $db->result();
    $output = '<ul>';

    if (empty($arr)) {
        $output .= '<li>No new items</li>';
    }

    foreach ($arr as $r) {
        $output .= '<li><a href="javascript:void(0);" onclick="loadNewsModal(' . $r['news_id'] . ')">' . $r['title'] . '</a></li>';
    }
    $output .= "</ul>";
    echo $output;
}

function events_list($class, $limit) {
    $db = Xcrud_db::get_instance();

    $select = "SELECT DISTINCT portal_calendar.* FROM portal_calendar 
    JOIN portal_calendar_class_join ON (portal_calendar_class_join.calendar_id = portal_calendar.calendar_id) 
    WHERE portal_calendar_class_join.class_id = $class AND (start_date >= NOW() AND end_date >= NOW()) ORDER BY start_date ";
    if ($limit > 0) {
        $select .= " LIMIT " . $limit . " ";
    }
    $db->query($select);
    $arr = $db->result();
    $output = '<ul>';

    if (empty($arr)) {
        $output .= 'No upcoming events';
    }
    $curr_date = '';
    foreach ($arr as $r) {
        $open_date = new DateTime($r['start_date']);
        $open_date = date_format($open_date, 'l, F j, Y');
        if ($curr_date != $open_date) {
            $output .= '<li style="list-style-type: none;margin-left:-15px;"><b>' . $open_date . '</b></li>';
            $curr_date = $open_date;
        }
        $output .= '<li><a href="javascript:void(0);" onclick="loadCalModal(' . $r['calendar_id'] . ')">' . $r['title'] . '</a></li>';
    }
    $output .= '</ul>';
    echo $output;
}

function home_events() {
    $db = Xcrud_db::get_instance();

    $select = "SELECT DISTINCT portal_calendar.*
                FROM portal_calendar
                JOIN portal_calendar_class_join ON (portal_calendar_class_join.calendar_id = portal_calendar.calendar_id)
                WHERE portal_calendar_class_join.class_id IN (76,".$_SESSION['class_office'].",".$_SESSION['class_dept'].")
                AND (start_date >= NOW() AND end_date >= NOW()) ORDER BY start_date LIMIT 10; ";
     $db->query($select);
    $arr = $db->result();
    $output = '<ul>';

    if (empty($arr)) {
        $output .= 'No upcoming events';
    }
    $curr_date = '';
    foreach ($arr as $r) {
        $open_date = new DateTime($r['start_date']);
        $open_date = date_format($open_date, 'l, F j, Y');
        if ($curr_date != $open_date) {
            $output .= '<li style="list-style-type: none;margin-left:-15px;"><b>' . $open_date . '</b></li>';
            $curr_date = $open_date;
        }
        $output .= '<li><a href="javascript:void(0);" onclick="loadCalModal(' . $r['calendar_id'] . ')">' . $r['title'] . '</a></li>';
    }
    $output .= '</ul>';
    echo $output;
}

function floor_plans($id) {

    $db = Xcrud_db::get_instance();
    $select = "SELECT * FROM floor_plans WHERE office = " . $id . " ORDER BY floor";
    $db->query($select);
    $arr = $db->result();
    $output = "<ul>";
    foreach ($arr as $r) {
        $output .= "<li><a href='floor-plan?f=" . $r["floor_plan_id"] . "' title='" . $r["floor_plan_name"] . "' target='_blank'>" . $r["floor_plan_name"] . "</a></li>";
    }
    $output .= "</ul>";
    echo $output;
}

/* DATABASE & SITES */

function getCustomApplications($tkid) {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT app_name, url
            FROM custom_app_users
            JOIN custom_apps ON custom_apps.app_id = custom_app_users.app_id
            WHERE tkid = " . $tkid . " ORDER BY app_name";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        echo "<ul>";
        foreach ($arr as $row) {
            echo "<li><a href='" . $row["url"] . "' target='_blank'>" . $row["app_name"] . "</a></li>";
        }
        echo "</ul>";
    }
}

function xml_feed($url, $limit = 10) {
    //$headlines = simplexml_load_file('http://www.pepperlaw.com/articles.xml');
    $content = file_get_contents($url);
    $x = new SimpleXmlElement($content);
    $i = 0;
    //echo('<ul>');
    foreach ($x->channel->item as $entry) {
        $i++;
        echo "<li><a href='$entry->link' title='$entry->title' target='_blank'>" . $entry->title . "</a></li>";
        if ($i > $limit)
            break;
    }
    //echo('</ul>');
}

/* function admin_dir_list($dir) {

  echo '<a href="file://///pepperphp/' . $dir . '" class="btn btn-sm btn-default btn-admin" style="margin-top:-10px;"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Add/Edit Items in this Folder</a></br><br/>';
  } */

function weather($city, $zip) {
    echo '<a href="http://www.wunderground.com/cgi-bin/findweather/getForecast?query=zmw:' . $zip . '.1.99999&bannertypeclick=wu_clean2day" title="' . $city . ' Weather Forecast" target="_blank">';
    echo '<img src="http://weathersticker.wunderground.com/weathersticker/cgi-bin/banner/ban/wxBanner?bannertype=wu_clean2day_cond&zip=' . $zip . '" /></a>';
}

/* / Fall Associates */

function associate_table($year, $season) {

    $db = Xcrud_db::get_instance();
    $sql = "SELECT * from associates WHERE Year = '$year' AND Season = '$season' ORDER BY last_name";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        foreach ($arr as $row) {
            echo "<div id='item' class='assoc-photo " . strtolower(str_replace(" ", "-", $row['office'])) . "' style='margin-right:20px;'>";
            echo "<a href='javascript:void(0);' onclick='loadModal(" . $row["associate_id"] . ");'>";
            echo "<img src='firm-directory-photos/" . $row['photo'] . "' class='img-rounded' width='150px'/>";
            echo "<p>" . $row['first_name'] . " " . $row['last_name'] . "</p></a></div>";

            //Modal
            echo '<div id="modal' . $row['associate_id'] . '" class="modal fade" tabindex="-1" data-width="600" style="display: none;">';
            echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
            echo '<h4 class="modal-title" style="padding-left:8px;padding-top:5px;">' . $row['first_name'] . " " . $row['last_name'] . '</span></h4>';
            echo '</div>';
            echo '<div class="modal-body" id="myModal">';
            echo '<div class="row">';
            echo '<div class="col-md-6">';
            echo '<img src="firm-directory-photos/' . $row['photo'] . '" class="img-rounded" width="250px" id="Photo">';
            echo '</div>';
            echo '<div class="col-md-6">';
            echo '<p><h4>Office</h4>' . $row['office'] . '</p>';

            if ($row['practice_group'] != NULL) {
                echo '<p><h4>Practice Group</h4>' . $row['practice_group'] . '</p>';
            }

            echo '<p><h4>Law School</h4>' . $row['law_school'] . '</p>';

            if ($row['undergrad'] != NULL) {
                echo '<p><h4>Undergraduate</h4>' . $row['undergrad'] . '</p>';
            }

            if ($row['grad_school'] != NULL) {
                echo '<p><h4>Graduate School</h4>' . $row['grad_school'] . '</p>';
            }

            if ($row['other_education'] != NULL) {
                echo '<p><h4>Other Education</h4>' . $row['other_education'] . '</p>';
            }

            if ($row['law_review'] != NULL) {
                echo '<p><h4>Law Review</h4>' . $row['law_review'] . '</p>';
            }

            if ($row['graduation_year'] != NULL) {
                echo '<p><h4>Graduation Year</h4>' . $row['graduation_year'] . '</p>';
            }

            if ($row['biography'] != NULL) {
                echo '<p><h4>Biography</h4>' . $row['biography'] . '</p>';
            }

            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '<div class="modal-footer">';
            echo '<button type="button" id="btn_cancel" data-dismiss="modal" class="btn btn-default">Close</button>';
            echo '</div>';
            echo '</div>';
        }
    }
}

function get_associates_office_groups($year, $season) {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT DISTINCT office from associates WHERE year = '$year' AND season = '$season' ORDER BY office";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        echo '<button class="btn btn-default is-checked" data-filter="*">Show All</button>';
        foreach ($arr as $row) {
            echo '<button class="btn btn-default is-checked" data-filter=".' . strtolower(str_replace(" ", "-", $row['office'])) . '">' . $row['office'] . '</button>';
        }
    }
}

/* IT Porfolio */

function it_portfolio() {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT * from portal_list_items WHERE list_id = 11 ORDER BY text1";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        foreach ($arr as $row) {
            echo "<div class='item port " . strtolower(str_replace(" ", "-", $row['text2'])) . "'>";
            echo '<figure class="embed dark">';
            echo "<a href='javascript:void(0);' onclick='loadModal(" . $row['item_id'] . ");'>";
            echo "<img src='img/it-portfolio-images/" . $row['text3'] . "'/></a>";
            echo '<figcaption>' . $row['text1'] . '</figcaption>';
            echo '</figure>';
            echo '</div>';
        }
    }

    if ($arr != null) {
        foreach ($arr as $row) {
            echo '<div id="modal' . $row['item_id'] . '" class="modal fade" tabindex="-1" data-width="760" style="display: none;">';
            echo '<div class="modal-header">';
            echo '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
            echo '<h4 class="modal-title" style="padding-left:8px;padding-top:5px;">' . $row['text1'] . '</span></h4>';
            echo '</div>';
            echo '<div class="modal-body" id="myModal">';
            echo '<div class="row">';
            echo '<div class="col-md-12">';

            if ($row['date1'] != NULL) {
                echo '<p><h4>Date</h4>' . $row["date1"] . '</p>';
            }

            if ($row['text4'] != NULL) {
                echo '<p><h4>URL</h4><a href="' . $row['text4'] . '">' . $row['text4'] . '</a></p>';
            }

            if ($row['text5'] != NULL) {
                echo '<p><h4>Description</h4>' . $row['text5'] . '</p>';
            }

            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '<div class="modal-footer">';
            echo '<button type="button" id="btn_cancel" data-dismiss="modal" class="btn btn-default">Close</button>';
            echo '</div>';
            echo '</div>';
        }
    }
}

function get_it_portfolio_groups() {
    $db = Xcrud_db::get_instance();
    $sql = "SELECT DISTINCT text2 from portal_list_items WHERE list_id = 11";
    $db->query($sql);
    $arr = $db->result();
    if ($arr != null) {
        echo '<button id="portfolio_all" class="btn btn-default is-checked" data-filter="*">Show All</button>';
        foreach ($arr as $row) {
            echo '<button class="btn btn-default" data-filter=".' . strtolower(str_replace(" ", "-", $row['text2'])) . '">' . $row['text2'] . '</button>';
        }
    }
}

//XCRUD FUNCTIONS
function xcrud_home_newslist($id, $type) {
    $xcrud_newslist = Xcrud::get_instance();
    $xcrud_newslist->table('news');
    $xcrud_newslist->order_by('publish_date', 'desc');
    //$xcrud->where('expire_date >=', date("Y/m/d H:i:s"));
    //Add where's for Office, PG, Committee, etc
    $xcrud_newslist->limit(10);
    $xcrud_newslist->column_cut(1000);
    $xcrud_newslist->columns('title');
    $xcrud_newslist->fields('title,publish_date,description,attachment');
    $xcrud_newslist->column_pattern('attachment', '<a href="uploads/{attachment}">{attachment}</a>');
    $xcrud_newslist->unset_add();
    $xcrud_newslist->unset_edit();
    //$xcrud->unset_view();
    $xcrud_newslist->unset_remove();
    $xcrud_newslist->unset_csv();
    $xcrud_newslist->unset_search();
    $xcrud_newslist->unset_title();
    $xcrud_newslist->unset_print();
    $xcrud_newslist->unset_limitlist();
    $xcrud_newslist->unset_title();
    //$xcrud->unset_pagination();
    echo $xcrud_newslist->render();
}

function xcrud_pepper_users($type) {
    $serverName = DB_SERVER;
    $connectionInfo = array("UID" => DB_USER, "PWD" => DB_PASS, "Database" => DB_NAME);
    $conn = sqlsrv_connect($serverName, $connectionInfo);

    if ($conn === false) {
        die(print_r(sqlsrv_errors(), true));
    }

    $sql = "SELECT TimeKeeperID, DisplayName FROM FirmDirectory WHERE Active = 1 ORDER BY LastName";
    $stmt = sqlsrv_query($conn, $sql);
    if ($stmt === false) {
        die(print_r(sqlsrv_errors(), true));
    }
    $ret = array();
    while ($row = sqlsrv_fetch_array($stmt)) {
        if ($type == 'name') {
            //select stores OfficeName in DB
            //$ret += [$row["DisplayName"] => $row["DisplayName"]];
        } else {
            //select stores OfficeID in DB
            //$ret += [(string)$row["TimeKeeperID"] => $row["DisplayName"]];
        }
    }
    sqlsrv_free_stmt($stmt);
    return $ret;
}

/* Home Page */

function home_news_headlines() {
    $db = Xcrud_db::get_instance();

    $select = "SELECT DISTINCT portal_news.*
                FROM portal_news
                JOIN portal_news_class_join ON (portal_news_class_join.news_id = portal_news.news_id)
                WHERE portal_news_class_join.class_id IN (76," . $_SESSION['class_office'] . "," . $_SESSION['class_dept'] . ")
                AND (publish_date <= NOW() AND expire_date >= NOW()) ORDER BY publish_date DESC; ";
    $db->query($select);
    $arr = $db->result();
    $output = '<ul>';
    if (empty($arr)) {
        $output .= '<li>No new items</li>';
    }
    foreach ($arr as $r) {
        $output .= '<li><a href="javascript:void(0);" onclick="loadNewsModal(' . $r['news_id'] . ')">' . $r['title'] . '</a></li>';
    }
    $output .= "</ul>";
    echo $output;
}

function new_matter_report() {
    $db = Xcrud_db::get_instance();

    $select = "SELECT * from new_matter_report ORDER BY nmr_id DESC LIMIT 10; ";
    $db->query($select);
    $arr = $db->result();
    $output = '<ul>';
    foreach ($arr as $r) {
        $output .= '<li><a href="uploads/new_matter_report/' . $r['attachment'] . '" target="_blank">' . $r['title'] . '</a></li>';
    }
    $output .= '</ul>';
    echo $output;
}

function audit_inquiry($limit) {
    $list_id = 6;
    $ai = Xcrud::get_instance();
    $ai->table('portal_list_items');
    $ai->limit($limit);
    $ai->where('list_id=' . $list_id);
    $ai->order_by('date2', 'desc');
    $ai->change_type('int1', 'select', '', xcrudDropDownOptions('pepperUsers', 'TimekeeperID', 'DisplayName', true));
    $ai->change_type('int2', 'select', '', xcrudDropDownOptions('pepperUsers', 'TimekeeperID', 'DisplayName', true));
    $ai->column_callback('int1', 'getDisplayNameLinkFromTKID');
    $ai->column_callback('int2', 'getDisplayNameLinkFromTKID');
    $ai->columns('int1,int2,date1,date2,text1,text2,text3');
    $ai->fields('int1,int2,date1,date2,text1,text2,text3');
    $ai->label('int1', 'Responsibile Paralegal');
    $ai->label('int2', 'ALC Partner');
    $ai->label('text1', 'Corporations');
    $ai->label('text2', 'Description');
    $ai->label('text3', 'Status');
    $ai->label('date1', 'Date of Financials');
    $ai->label('date2', 'Due Date');
    $ai->pass_var('list_id', $list_id);
    $ai->unset_add();
    $ai->unset_edit();
    $ai->unset_view();
    $ai->unset_remove();
    $ai->unset_search();
    $ai->unset_title();
    echo $ai->render();
}

function firm_notaries() {
    $list_id = 1;
    $firm_notaries = Xcrud::get_instance();
    $firm_notaries->table('portal_list_items');
    $firm_notaries->where('list_id=' . $list_id);
    $firm_notaries->change_type('text2', 'select', '', xcrudDropDownOptions('pepperUsers', 'DisplayName', 'DisplayName', true));
    $firm_notaries->change_type('int2', 'select', '', xcrudDropDownOptions('allOffices', 'PKOfficeID', 'OfficeName', true));
    $firm_notaries->column_callback('text2', 'getDisplayNameLinkFromFullName');
    $firm_notaries->columns('text2,int2');
    $firm_notaries->fields('text2,int2');
    $firm_notaries->label('text2', 'Person');
    $firm_notaries->label('int2', 'Office');
    $firm_notaries->pass_var('list_id', $list_id);
    $firm_notaries->limit(1000);
    $firm_notaries->order_by('int2,text2');
    if (!$_SESSION["admin_visible"]) {
        $firm_notaries->unset_add();
        $firm_notaries->unset_edit();
        $firm_notaries->unset_remove();
    }
    $firm_notaries->unset_title();
    $firm_notaries->unset_pagination();
    echo $firm_notaries->render();
}

function office_support($office) {
    $office_support = Xcrud::get_instance();
    $office_support->table('office_support_list');
    $office_support->where("office = '" . $office . "'");
    $office_support->pass_var("office", $office);
    $office_support->columns("function_name,contact,comments");
    $office_support->fields("function_name,contact,comments");
    $office_support->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $office_support->unset_add();
        $office_support->unset_edit();
        $office_support->unset_remove();
    }
    $office_support->unset_title();
    echo $office_support->render();
}

function visitor_log($office) {
    $visitor_log = Xcrud::get_instance();
    $visitor_log->table('visitor_log');
    $visitor_log->where("visitor_office = '" . $office . "'");
    $visitor_log->pass_var("visitor_office", $office);
    $visitor_log->fields("visitor_name,date_time_in,date_time_out,company_name,pepper_host");
    $visitor_log->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $visitor_log->unset_add();
        $visitor_log->unset_edit();
        $visitor_log->unset_view();
        $visitor_log->unset_remove();
    }
    $visitor_log->unset_title();
    $visitor_log->order_by('date_time_in','desc');
    echo $visitor_log->render();
}

function firm_holidays() {
    $list_id = 2;
    $firm_holidays = Xcrud::get_instance();
    $firm_holidays->table('portal_list_items');
    $firm_holidays->where('list_id=' . $list_id);
    $firm_holidays->columns('date1,text1');
    $firm_holidays->fields('date1,text1');
    $firm_holidays->label('date1', 'Date');
    $firm_holidays->label('text1', 'Title');
    $firm_holidays->pass_var('list_id', $list_id);
    $firm_holidays->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $firm_holidays->unset_add();
        $firm_holidays->unset_edit();
        $firm_holidays->unset_view();
        $firm_holidays->unset_remove();
    }
    $firm_holidays->unset_title();
    $firm_holidays->unset_search();
    $firm_holidays->unset_pagination();
    $firm_holidays->unset_limitlist();
    echo $firm_holidays->render();
}

function reads_writes_speaks() {
    $list_id = 3;
    $rws = Xcrud::get_instance();
    $rws->table('portal_list_items');
    $rws->where('list_id=' . $list_id);
    $rws->change_type('text2', 'select', '', xcrudDropDownOptions('pepperUsers', 'DisplayName', 'DisplayName', true));
    $rws->columns('text2,text1,yn1,yn2,yn3');
    $rws->fields('text2,text1,yn1,yn2,yn3');
    $rws->column_callback('text2', 'getDisplayNameLinkFromFullName');
    $rws->label('text1', 'Language');
    $rws->label('text2', 'Person');
    $rws->label('yn1', 'Reads');
    $rws->label('yn2', 'Writes');
    $rws->label('yn3', 'Speaks');
    $rws->pass_var('list_id', $list_id);
    $rws->search_columns('text1');
    $rws->order_by('text2');
    $rws->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $rws->unset_add();
        $rws->unset_edit();
        $rws->unset_view();
        $rws->unset_remove();
    }
    $rws->unset_title();
    echo $rws->render();
}

/* PRO BONO */

function pro_bono_opp() {
    $list_id = 20;
    $pro_bono = Xcrud::get_instance();
    $pro_bono->table('portal_list_items');
    $pro_bono->where('list_id=' . $list_id);
    $pro_bono->columns('text1,text2,text3,text4,text5,text6');
    $pro_bono->fields('text1,text2,text3,text4,text5,text6');
    $pro_bono->label('text1', 'Title');
    $pro_bono->label('text2', 'Office');
    $pro_bono->label('text3', 'Department');
    $pro_bono->label('text4', 'Description');
    $pro_bono->label('text5', 'Time');
    $pro_bono->label('text6', 'Pepper Contact');
    $pro_bono->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $pro_bono->unset_add();
        $pro_bono->unset_edit();
        $pro_bono->unset_view();
        $pro_bono->unset_remove();
    }
    $pro_bono->unset_title();
    echo $pro_bono->render();
}

/* HR */

function hr_jobs() {
    $list_id = 44;
    $hr_jobs = Xcrud::get_instance();
    $hr_jobs->table('portal_list_items');
    $hr_jobs->where('list_id=' . $list_id);
    $hr_jobs->columns('text1,desc1,int1,text3,date1,text4');
    $hr_jobs->fields('text1,desc1,int1,text3,date1,text4');
    $hr_jobs->change_type('int1', 'select', '', xcrudDropDownOptions('allOffices', 'PKOfficeID', 'OfficeName', true));
    $hr_jobs->label('text1', 'Job Title');
    $hr_jobs->label('desc1', 'Responsibilities');
    $hr_jobs->label('text3', 'Deptartment Practice Group');
    $hr_jobs->label('date1', 'Date Posted');
    $hr_jobs->label('text4', 'Qualifications');
    $hr_jobs->label('int1', 'Office');
    $hr_jobs->change_type('text4', 'textarea');
    $hr_jobs->set_attr('text4', array('rows' => 10));
    $hr_jobs->order_by('date1', 'desc');

    $hr_jobs->pass_var('list_id', $list_id);
    if (!$_SESSION["admin_visible"]) {
        $hr_jobs->unset_add();
        $hr_jobs->unset_edit();
        $hr_jobs->unset_view();
        $hr_jobs->unset_remove();
    }
    $hr_jobs->unset_title();
    echo $hr_jobs->render();
}

function personnel_announcements() {
    $list_id = 10;
    $pann = Xcrud::get_instance();
    $pann->table('portal_list_items');
    $pann->where('list_id=' . $list_id);
    $pann->columns('text1,text2');
    $pann->fields('text1,text2');
    $pann->change_type('text2', 'textarea');
    $pann->set_attr('text2', array('rows' => 10));
    $pann->label('text1', 'Title');
    $pann->label('text2', 'Body');
    $pann->column_cut(1000);
    $pann->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $pann->unset_edit(false);
        $pann->unset_remove(false);
    }
    $pann->unset_title();
    echo $pann->render();
}

function unidentified_mail() {
    $list_id = 43;
    $imail = Xcrud::get_instance();
    $imail->table('portal_list_items');
    $imail->where('list_id=' . $list_id);
    $imail->fields('text1,date1,text2,text3');
    $imail->columns('text1,date1,text2,text3');
    $imail->label('text1', 'Title');
    $imail->label('date1', 'received');
    $imail->label('text2', 'Mail Location');
    $imail->label('text3', 'Description');
    $imail->pass_var('list_id', $list_id);
    if (!$_SESSION["admin_visible"]) {
        $imail->unset_add();
        $imail->unset_edit();
        $imail->unset_view();
        $imail->unset_remove();
    }
    $imail->unset_title();
    echo $imail->render();
}

/* FIRM */

function new_hires($office) {
    $method = "newHires/" . $office;
    $json_array = getCURL($method);
    if ($json_array != null) {
        echo "<ul id='new-hires'>";
        foreach ($json_array as $row) {
            if (file_exists("firm-directory-photos/" . $row->Username . ".jpg")) {
                echo "<li style='border:0'><table><tr><td style='width:150px;border:none;'><a href='people/" . $row->Username . "'><img src='firm-directory-photos/" . $row->Username . ".jpg' class='img-rounded' style='width:100px;'/></a></td>";
            } else {
                echo "<li style='border:0'><table><tr><td style='width:150px;border:none;'><a href='people/" . $row->Username . "'><img src='assets/img/user-icon.png' class='img-rounded' style='width:100px;'/></a></td>";
            }
            echo "<td style='border:none;'><a href='people/" . $row->Username . "'>" . $row->DisplayName . "</a><br/>";
            echo $row->Extension . "<br/>";
            echo $row->Position . "<br/>";
            echo $row->PracticeGroup . "</li></td></tr></table>";
        }
        echo "</ul>";
        echo "<br/>";
    } else {
        echo '<ul><li>No recent hires</li></ul>';
    }
}

function bar_admissions_outside() {
    $list_id = 46;
    $bar = Xcrud::get_instance();
    $bar->table('portal_list_items');
    $bar->where('list_id=' . $list_id);
    $bar->change_type('int1', 'select', '', xcrudDropDownOptions('pepperUsers', 'TimekeeperID', 'DisplayName', true));
    $bar->column_callback('int1', 'getDisplayNameLinkFromTKID');
    $bar->columns('text1,int1');
    $bar->fields('text1,int1');
    $bar->label('int1', 'Person');
    $bar->label('text1', 'Jurisdiction');
    $bar->pass_var('list_id', $list_id);
    $bar->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $bar->unset_add();
        $bar->unset_edit();
        $bar->unset_view();
        $bar->unset_remove();
    }
    $bar->unset_title();
    echo $bar->render();
}

function peppourri() {
    $list_id = 4;
    $peppourri = Xcrud::get_instance();
    $peppourri->table('portal_list_items');
    $peppourri->where('list_id=' . $list_id);
    $peppourri->columns('text1,text5,text2,text3,text4,desc1,desc1,attachment,date1');
    $peppourri->fields('text1,text5,text2,text3,text4,desc1,desc1,attachment,date1');
    $peppourri->label('text1', 'Title');
    $peppourri->label('desc1', 'Description');
    $peppourri->label('text5', 'Contact Name');
    $peppourri->label('text2', 'Contact Email');
    $peppourri->label('text3', 'Contact Phone');
    $peppourri->label('text4', 'Location');
    $peppourri->label('date1', 'Post Date');
    $peppourri->label('attachment', 'Photo');
    $peppourri->pass_var('list_id', $list_id);
    $peppourri->pass_var('int1', $_SESSION['tkid']);
    $peppourri->order_by('date1','desc');
    if (!$_SESSION['admin_visible']) {
        $peppourri->unset_edit(false, 'int1', '=', $_SESSION['tkid']);
        $peppourri->unset_remove(false, 'int1', '=', $_SESSION['tkid']);
    }
    $peppourri->change_type('attachment', 'image', '', array('not_rename' => false, 'path' => '../uploads/peppourri', 'width' => 300));
    $peppourri->unset_title();


    echo $peppourri->render();
}

/* BENEFITS */

function transportation_links() {
    echo '<h2>Transportation Links</h2>';
    $list_id = 53;
    $transportation_links = Xcrud::get_instance();
    $transportation_links->where('list_id=' . $list_id);
    $transportation_links->table('portal_list_items');
    $transportation_links->order_by('text1');
    $transportation_links->limit(25);
    $transportation_links->column_cut(1000);
    $transportation_links->pass_var('list_id', $list_id);
    $transportation_links->columns('text1,text2,url1');
    $transportation_links->fields('text1,text2,url1');
    $transportation_links->label('text1', 'Title');
    $transportation_links->label('text2', 'Comments');
    $transportation_links->label('url1', 'URL');
    $transportation_links->change_type('text2', 'textarea');
    $transportation_links->label('date1', 'Date Posted');
    if (!$_SESSION['admin_visible']) {
        $transportation_links->unset_add();
        $transportation_links->unset_edit();
        $transportation_links->unset_remove();
    }
    $transportation_links->unset_csv();
    $transportation_links->unset_search();
    $transportation_links->unset_title();
    $transportation_links->unset_print();
    $transportation_links->unset_title();
    $transportation_links->unset_limitlist();
    echo $transportation_links->render();
}

/* LIBRARY */

function library_desk_book() {
    $list_id = 57;
    $lib_desk_book = Xcrud::get_instance();
    $lib_desk_book->table('portal_list_items');
    $lib_desk_book->where('list_id=' . $list_id);
    $lib_desk_book->columns('text1,text2');
    $lib_desk_book->fields('text1,text2');
    $lib_desk_book->label('text1', 'Group');
    $lib_desk_book->label('text2', 'Resources');

    $lib_desk_book->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $lib_desk_book->unset_add();
        $lib_desk_book->unset_edit();
        $lib_desk_book->unset_remove();
    }
    $lib_desk_book->unset_title();
    $lib_desk_book->unset_csv();
    $lib_desk_book->unset_search();
    $lib_desk_book->unset_title();
    $lib_desk_book->unset_print();
    $lib_desk_book->unset_limitlist();
    echo $lib_desk_book->render();
}

function lib_hot_tip() {
    $list_id = 14;
    $hot_tip = Xcrud::get_instance();
    $hot_tip->table('portal_list_items');
    $hot_tip->where('list_id=' . $list_id);
    $hot_tip->columns('text1');
    $hot_tip->fields('text1');
    $hot_tip->label('text1', 'Tip');
    $hot_tip->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $hot_tip->unset_add();
        $hot_tip->unset_edit();
        $hot_tip->unset_remove();
    }
    $hot_tip->unset_title();
    $hot_tip->unset_csv();
    $hot_tip->unset_search();
    $hot_tip->unset_title();
    $hot_tip->unset_print();
    $hot_tip->unset_limitlist();
    echo $hot_tip->render();
}

function ask_a_librarian() {
    $list_id = 12;
    $ask_lib = Xcrud::get_instance();
    $ask_lib->table('portal_list_items');
    $ask_lib->where('list_id=' . $list_id);
    $ask_lib->columns('text1,text2,text3');
    $ask_lib->fields('text1,text2,text3');
    $ask_lib->label('text1', 'Office');
    $ask_lib->label('text2', 'Phone');
    $ask_lib->label('text3', 'Email');
    $ask_lib->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $ask_lib->unset_add();
        $ask_lib->unset_edit();
        $ask_lib->unset_remove();
    }
    $ask_lib->unset_title();
    echo $ask_lib->render();
}

/* TRAVEL */

function travel_arrangements_list() {
    $list_id = 23;
    $travel_arrangements_list = Xcrud::get_instance();
    $travel_arrangements_list->table('portal_list_items');
    $travel_arrangements_list->where('list_id=' . $list_id);
    $travel_arrangements_list->columns('text1,text2,text3');
    $travel_arrangements_list->fields('text1,text2,text3');
    $travel_arrangements_list->label('text1', 'Title');
    $travel_arrangements_list->label('text2', 'Contact');
    $travel_arrangements_list->label('text3', 'Extension');
    $travel_arrangements_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_arrangements_list->unset_add();
        $travel_arrangements_list->unset_edit();
        $travel_arrangements_list->unset_remove();
    }
    $travel_arrangements_list->unset_title();
    $travel_arrangements_list->limit(50);
    $travel_arrangements_list->unset_pagination();
    $travel_arrangements_list->unset_search();
    $travel_arrangements_list->unset_limitlist();
    $travel_arrangements_list->column_cut(500);
    echo $travel_arrangements_list->render();
}

function travel_hotel_chain_list() {
    $list_id = 24;
    $travel_hotel_chain_list = Xcrud::get_instance();
    $travel_hotel_chain_list->table('portal_list_items');
    $travel_hotel_chain_list->where('list_id=' . $list_id);
    $travel_hotel_chain_list->columns('text1,text2');
    $travel_hotel_chain_list->fields('text1,text2');
    $travel_hotel_chain_list->label('text1', 'Hotel');
    $travel_hotel_chain_list->label('text2', 'Phone Number');
    $travel_hotel_chain_list->order_by('text1');
    $travel_hotel_chain_list->limit(50);
    $travel_hotel_chain_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_hotel_chain_list->unset_add();
        $travel_hotel_chain_list->unset_edit();
        $travel_hotel_chain_list->unset_remove();
    }
    $travel_hotel_chain_list->unset_title();
    $travel_hotel_chain_list->unset_pagination();
    $travel_hotel_chain_list->unset_search();
    $travel_hotel_chain_list->unset_limitlist();
    echo $travel_hotel_chain_list->render();
}

function travel_hotels_by_office() {
    $list_id = 25;
    $travel_hotels_by_office = Xcrud::get_instance();
    $travel_hotels_by_office->table('portal_list_items');
    $travel_hotels_by_office->where('list_id=' . $list_id);
    $travel_hotels_by_office->columns('text1,text2,text3,text4');
    $travel_hotels_by_office->fields('text1,text2,text3,text4');
    $travel_hotels_by_office->label('text1', 'Office');
    $travel_hotels_by_office->label('text2', 'Hotel');
    $travel_hotels_by_office->label('text3', 'Address');
    $travel_hotels_by_office->label('text4', 'Phone Number');
    $travel_hotels_by_office->limit(50);
    $travel_hotels_by_office->order_by('text1');
    $travel_hotels_by_office->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_hotels_by_office->unset_add();
        $travel_hotels_by_office->unset_edit();
        $travel_hotels_by_office->unset_remove();
    }
    $travel_hotels_by_office->unset_title();
    $travel_hotels_by_office->unset_pagination();
    $travel_hotels_by_office->unset_search();
    $travel_hotels_by_office->unset_limitlist();
    echo $travel_hotels_by_office->render();
}

function travel_train_list() {
    $list_id = 28;
    $travel_train_list = Xcrud::get_instance();
    $travel_train_list->table('portal_list_items');
    $travel_train_list->where('list_id=' . $list_id);
    $travel_train_list->columns('text1,text2');
    $travel_train_list->fields('text1,text2');
    $travel_train_list->label('text1', 'Train');
    $travel_train_list->label('text2', 'Phone Number');
    $travel_train_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_train_list->unset_add();
        $travel_train_list->unset_edit();
        $travel_train_list->unset_remove();
    }
    $travel_train_list->unset_title();
    $travel_train_list->unset_pagination();
    $travel_train_list->unset_search();
    $travel_train_list->unset_limitlist();
    echo $travel_train_list->render();
}

function travel_airline_list() {
    $list_id = 22;
    $travel_airline_list = Xcrud::get_instance();
    $travel_airline_list->table('portal_list_items');
    $travel_airline_list->where('list_id=' . $list_id);
    $travel_airline_list->columns('text1,text2,text3');
    $travel_airline_list->fields('text1,text2,text3');
    $travel_airline_list->label('text1', 'Name');
    $travel_airline_list->label('text2', 'Code');
    $travel_airline_list->label('text3', 'Phone');
    $travel_airline_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_airline_list->unset_add();
        $travel_airline_list->unset_edit();
        $travel_airline_list->unset_remove();
    }
    $travel_airline_list->unset_title();
    $travel_airline_list->unset_pagination();
    $travel_airline_list->unset_search();
    $travel_airline_list->unset_limitlist();
    echo $travel_airline_list->render();
}

function travel_limo_list() {
    $list_id = 26;
    $travel_limo_list = Xcrud::get_instance();
    $travel_limo_list->table('portal_list_items');
    $travel_limo_list->where('list_id=' . $list_id);
    $travel_limo_list->columns('text1,text2');
    $travel_limo_list->fields('text1,text2');
    $travel_limo_list->label('text1', 'Name');
    $travel_limo_list->label('text2', 'Phone');
    $travel_limo_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_limo_list->unset_add();
        $travel_limo_list->unset_edit();
        $travel_limo_list->unset_remove();
    }
    $travel_limo_list->unset_title();
    $travel_limo_list->unset_pagination();
    $travel_limo_list->unset_search();
    $travel_limo_list->unset_limitlist();
    echo $travel_limo_list->render();
}

function travel_rental_car_list() {
    $list_id = 27;
    $travel_rental_car_list = Xcrud::get_instance();
    $travel_rental_car_list->table('portal_list_items');
    $travel_rental_car_list->where('list_id=' . $list_id);
    $travel_rental_car_list->columns('text1,text2');
    $travel_rental_car_list->fields('text1,text2');
    $travel_rental_car_list->label('text1', 'Name');
    $travel_rental_car_list->label('text2', 'Phone');
    $travel_rental_car_list->where('list_id=' . $list_id);
    $travel_rental_car_list->pass_var('list_id', $list_id);
    if (!$_SESSION['admin_visible']) {
        $travel_rental_car_list->unset_add();
        $travel_rental_car_list->unset_edit();
        $travel_rental_car_list->unset_remove();
    }
    $travel_rental_car_list->unset_title();
    $travel_rental_car_list->unset_pagination();
    $travel_rental_car_list->unset_search();
    $travel_rental_car_list->unset_limitlist();
    echo $travel_rental_car_list->render();
}

/* FIRM POLICIES */

function firm_policies_staff() {
    echo '<h2>New and Revised Staff Policies</h2>';
    $list_id = 9;
    $firm_policies_staff = Xcrud::get_instance();
    $firm_policies_staff->where('list_id=' . $list_id);
    $firm_policies_staff->table('portal_list_items');
    $firm_policies_staff->order_by('date1', 'desc');
    $firm_policies_staff->limit(25);
    $firm_policies_staff->column_cut(1000);
    $firm_policies_staff->pass_var('list_id', $list_id);
    $firm_policies_staff->columns('text1,text2,date1');
    $firm_policies_staff->fields('text1,text2,date1');
    $firm_policies_staff->label('text1', 'Revision');
    $firm_policies_staff->label('text2', 'Description');
    $firm_policies_staff->label('date1', 'Date Posted');
    if (!$_SESSION['admin_visible']) {
        $firm_policies_staff->unset_add();
        $firm_policies_staff->unset_edit();
        $firm_policies_staff->unset_remove();
    }
    $firm_policies_staff->unset_csv();
    $firm_policies_staff->unset_search();
    $firm_policies_staff->unset_title();
    $firm_policies_staff->unset_print();
    $firm_policies_staff->unset_title();
    $firm_policies_staff->unset_limitlist();
    echo $firm_policies_staff->render();
}

/* PROFESSIONAL DEVELOPMENT */

function pd_resources() {
    echo '<h2>PD Resources</h2>';
    $list_id = 54;
    $pd_resources = Xcrud::get_instance();
    $pd_resources->where('list_id=' . $list_id);
    $pd_resources->table('portal_list_items');
    $pd_resources->order_by('text4');
    $pd_resources->limit(25);
    $pd_resources->column_cut(1000);
    $pd_resources->pass_var('list_id', $list_id);
    $pd_resources->columns('text1,text2,text3,text4');
    $pd_resources->fields('text1,text2,text3,text4');
    $pd_resources->label('text1', 'Title');
    $pd_resources->label('text2', 'Author');
    $pd_resources->label('text3', 'Media');
    $pd_resources->label('text4', 'Resource Type');
    if (!$_SESSION['admin_visible']) {
        $pd_resources->unset_add();
        $pd_resources->unset_edit();
        $pd_resources->unset_remove();
    }
    $pd_resources->unset_title();
    $pd_resources->unset_limitlist();
    echo $pd_resources->render();
}

/* PRACTICE GROUPS */

function ip_practice_group_names(){
    $list_id = 69;
    $ippg = Xcrud::get_instance();
    $ippg->table('portal_list_items');
    $ippg->where('list_id=' . $list_id);
    $ippg->change_type('int1', 'select', '', xcrudDropDownOptions('pepperUsers', 'TimekeeperID', 'DisplayName', true));
    $ippg->change_type('int2', 'select', '', xcrudDropDownOptions('pepperUsers', 'TimekeeperID', 'DisplayName', true));
    $ippg->column_callback('int1', 'getDisplayNameLinkFromTKID');
    $ippg->column_callback('int2', 'getDisplayNameLinkFromTKID');
    $ippg->column_callback('int2', 'getDisplayNameLinkFromTKID');
    $ippg->columns('text1,int1,int2');
    $ippg->fields('text1,int1,int2');
    $ippg->label('int1', 'Leadership');
    $ippg->label('int2', 'Leadership');
    $ippg->label('text1', 'Practice Group');
    $ippg->column_pattern('text1','<a href="{url1}">{text1}</a>');
    $ippg->limit(100);
    if (!$_SESSION["admin_visible"]) {
        $ippg->unset_add();
        $ippg->unset_edit();
        $ippg->unset_view();
        $ippg->unset_remove();
    }
    $ippg->unset_title();
    $ippg->unset_csv();
    $ippg->unset_print();
    $ippg->unset_search();
    $ippg->unset_limitlist();
    $ippg->column_cut(500);
    
    echo $ippg->render();
}

function google_news_headlines($pg) {
    $db = Xcrud_db::get_instance();

    $select = "SELECT * FROM practice_group_search_terms WHERE class_id = " . $pg . " AND active = 1 ORDER BY sort";
    $db->query($select);
    $arr = $db->result();
    if ($_SESSION["admin_visible"]) {
        echo "<a class='btn btn-sm btn-admin btn-default' title='[ADMIN] Add/Edit Search Terms' target='_blank' class='btn btn-default btn-xs btn-admin' href='admin/admin-practice-group-headline-terms.php?p=" .
        $pg . "' target='_blank'><span class='glyphicon glyphicon-link'></span></a></br/>";
    }
    $output = '<ul>';
    foreach ($arr as $row) {
        if ($row['exact_match']) {
            //$url = "http://news.google.com/news?q=" . "\"" . $row['SearchTerm'] . "\"" . "&output=rss&ned=us";
            $url = "http://news.google.com/news?q=" . preg_replace("/ /", "+", $row['search_term']) . "&output=rss&ned=us";
        } else {
            $url = "http://news.google.com/news?q=" . preg_replace("/ /", "+", $row['search_term']) . "&output=rss&ned=us";
        }
        //$headlines = simplexml_load_file('http://www.pepperlaw.com/articles.xml');
        //$content = file_get_contents('https://news.google.com/news/feeds?q=National+Bankruptcy+Services&output=rss&ned=us');

        $content = file_get_contents($url);
        $x = new SimpleXmlElement($content);
        $i = 0;
        $output = '';
        foreach ($x->channel->item as $entry) {
            $i++;
            $output .= "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
            if ($i > 10)
                break;
        }
    }
    $output .= "</ul>";
    echo $output;
}

function google_client_headlines($client) {
    $db = Xcrud_db::get_instance();

    $select = "SELECT * FROM client_headline_terms WHERE client_number = " . $client . " AND active = 1 ORDER BY sort";
    $db->query($select);
    $arr = $db->result();
    if ($_SESSION["admin_visible"]) {
        echo "<a class='btn btn-sm btn-admin btn-default' title='[ADMIN] Add/Edit Headline Terms' target='_blank' class='btn btn-default btn-xs btn-admin' href='admin/admin-client-headlines.php?id=" .
        $client . "' target='_blank'><span class='glyphicon glyphicon-edit'></span> EDIT TERMS</a></br/>";
    }
    $output = '<ul>';
    foreach ($arr as $row) {
        if ($row['exact_match']) {
            //$url = "http://news.google.com/news?q=" . "\"" . $row['SearchTerm'] . "\"" . "&output=rss&ned=us";
            $url = "http://news.google.com/news?q=" . preg_replace("/ /", "+", $row['search_term']) . "&output=rss&ned=us";
        } else {
            $url = "http://news.google.com/news?q=" . preg_replace("/ /", "+", $row['search_term']) . "&output=rss&ned=us";
        }
        //$headlines = simplexml_load_file('http://www.pepperlaw.com/articles.xml');
        //$content = file_get_contents('https://news.google.com/news/feeds?q=National+Bankruptcy+Services&output=rss&ned=us');

        $content = file_get_contents($url);
        $x = new SimpleXmlElement($content);
        $i = 0;
        $output = '';
        foreach ($x->channel->item as $entry) {
            $i++;
            $output .= "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
            if ($i > 10)
                break;
        }
    }
    $output .= "</ul>";
    echo $output;
}

function pg_lib_links($pg, $cat) {
    $db = Xcrud_db::get_instance();

    $select = "SELECT DISTINCT library_links.url, library_links.title, library_links.link_id
                FROM library_link_pg_join
                JOIN library_links ON library_links.link_id = library_link_pg_join.link_id
                WHERE pg_id = '$pg' AND category='$cat' ORDER BY Title";
    $db->query($select);
    $arr = $db->result();
    $output = '';

    if ($arr != null) {
        $output = "<h2>" . $cat . "</h2>";
        $output .= '<ul>';
        foreach ($arr as $row) {
            $output .= "<li><a href='" . $row['url'] . "' target='_blank'>" . $row['title'] . "</a></li>";
        }
        $output .= "</ul>";
    }
    echo $output;
}

function remoteFileExists($url) {
    $curl = curl_init($url);

    //don't fetch the actual page, you only want to check the connection is ok
    curl_setopt($curl, CURLOPT_NOBODY, true);

    //do request
    $result = curl_exec($curl);

    $ret = false;

    //if request did not fail
    if ($result !== false) {
        //if request was ok, check response code
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($statusCode == 200) {
            $ret = true;
        }
    }

    curl_close($curl);

    return $ret;
}

/* PEOPLE TABLES */

function dt_people_table() {
    echo '<table id="example" class="display" cellspacing="0" width="100%">';
    echo '<thead>';
    echo '<tr>';
    echo '<th width="100px">&nbsp;</th>';
    echo '<th width="20%">Name</th>';
    echo '<th width="20%">Title</th>';
    echo '<th width="20%">Email</th>';
    echo '<th width="20%">Extension</th>';
    echo '<th width="20%">Office</th>';
    echo '</tr>';
    echo '</thead>';
    echo '</table>';
}

function people_table($method) {
    $json_array = getCURL($method);
    echo '<table class="table-no-lines';
    if (!strpos($method, "PeopleTableDynamic")) {
        echo ' datatable';
    }
    echo '" id="people-table">';
    echo '<thead>';
    echo '<tr>';
    echo '<th style="width:75px;">&nbsp</th>';
    echo '<th style="width:200px;">Name</th>';
    echo '<th style="width:200px;">Title</th>';
    echo '<th style="text-align:center;width:40px;">Email</th>';
    echo '<th style="width:100px;">Extension</th>';
    echo '<th style="width:100px;">Office</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    $role_name = "";
    foreach ($json_array as $row) {
        if (strpos($method, "Committee")) {
            if ($role_name != $row->RoleName) {
                echo ("<tr><td colspan='6'><h2 style='border-bottom:0;margin-bottom:-5px;'>" . $row->RoleName . "</h2></td></tr>");
                $role_name = $row->RoleName;
            }
        }

        if (file_exists("firm-directory-photos/" . $row->Username . ".jpg")) {
            echo "<tr><td style='text-align:center;width:75px;'><a href='people/" . $row->Username . "'><img src='firm-directory-photos/" . $row->Username . ".jpg' class='img-rounded'    style='width:75px'/></a></td>";
        } else {
            echo "<tr><td style='text-align:center;width:75px;'><span class='glyphicon glyphicon-user big-glyph'></span></td>";
        }

        echo "<td><a href='people/" . $row->Username . "'>" . $row->DisplayName . "</a></td>";
        echo "<td style='width:200px;'>" . $row->Position . "</td>";
        echo "<td style='text-align:center;width:40px;'><a href='mailto:" . $row->Username . "@pepperlaw.com' title='Email " . $row->DisplayName . "'><span class='glyphicon glyphicon-envelope med-glyph'></span></a></td>";
        echo "<td style='width:100px;'>" . $row->Extension . "</td>";
        echo "<td style='width:100px;'>" . $row->Office . "</td></tr>";
    }
    echo '</tbody>';
    echo '</table>';
}

function getCommitteesforPerson($tkid) {
    $json_array = getCURL('getCommitteesByTKID/' . $tkid);
    if ($json_array != null) {
        echo '<tr><td class="form-head">Committee(s):</td><td>';
        foreach ($json_array as $row) {
            echo '' . $row->CommitteeName;
            if ($row->RoleName != 'Member')
                echo ' (' . $row->RoleName . ')<br/>';
            else
                echo '<br/>';
        }
        echo '</td></tr>';
    }
}

/* ELITE */

function getMattersforPG($dept) {
    $json_array = getCURL_elite('getMattersByPG/' . $dept);
    echo '<table class="table table-condensed" id="matters-table">';
    echo '<tr>';
    echo '<th>Client Name</th>';
    echo '<th>Client#</th>';
    echo '<th>Matter Name</th>';
    echo '<th>Matter#</th>';
    echo '<th>Billing Attorney</th>';
    echo '<th>Open Date</th>';
    foreach ($json_array as $row) {
        echo "<tr>";
        echo "<td><a href='client/" . $row->cnum . "'>" . $row->cname . "</a></td>";
        echo "<td><a href='client/" . $row->cnum . "'>" . $row->cnum . "</a></td>";
        echo "<td><a href='matter/" . $row->mmatter . "'>" . $row->mdesc1 . "</a></td>";
        echo "<td><a href='matter/" . $row->mmatter . "'>" . $row->mnum . "</a></td>";
        echo "<td><a href='people/" . $row->account . "'>" . $row->baname . "</a></td>";
        $open_date = new DateTime($row->mopendt->date);
        echo "<td>" . date_format($open_date, 'm/d/y') . "</td></tr>";
    }
    echo '</table>';
}

function get_client_name($clientnum) {
    $json_array = getCURL_elite('getClientName/' . $clientnum);
    $ret = "";
    foreach ($json_array as $row) {
        $ret = $row->client_num_name;
    }
    return $ret;
}

function get_client_table($clientnum) {
    $json_array = getCURL_elite('getClientInfo/' . $clientnum);
    foreach ($json_array as $row) {
        echo '<table class="table table-condensed">';
        echo '<tr><td class="form-head">Client:</td>';
        echo '<td>' . $row->clname1 . ' [' . $row->clnum . ']</td></tr>';
        echo '<tr><td class="form-head">Address:</td>';
        echo '<td>' . $row->claddr1 . '<br/>' . $row->claddr2 . '<br/>' . $row->claddr3 . '</td></tr>';
        echo '<tr><td class="form-head">Phone:</td>';
        echo '<td>' . $row->clphone . '</td></tr>';
        echo '<tr><td class="form-head">Fax:</td>';
        echo '<td>' . $row->clfax . '</td></tr>';
        echo '<tr><td class="form-head">Status:</td><td>';
        if ($row->clstatus == 'C')
            echo 'Current';
        else
            echo 'Inactive';
        echo '</td></tr>';
        echo '<tr><td class="form-head">Open Date:</td>';
        echo '<td>' . date("m/d/Y", strtotime($row->clopendt->date)) . '</td>';
        echo '<tr><td class="form-head">Office:</td>';
        echo '<td>' . $row->office . '</td></tr>';
        echo '<tr><td class="form-head">Related Client:</td>';
        echo '<td><a href="client/' . $row->crelated . ' ">' . $row->crelatedname . '</a> [' . $row->crelated . ']</td></tr>';
        echo '</table>';
    }
}

function get_matter_name($mattername) {
    $json_array = getCURL_elite('getMatterName/' . $mattername);
    foreach ($json_array as $row) {
        echo $row->mname . " [" . $row->mnum . ']';
        echo "<br/><a href='client/" . $row->cnum . "'>" . $row->cname . " [" . $row->cnum . "]</a>";
    }
}

function get_top_biller($clientnum) {
    $json_array = getCURL_elite('getTopBiller/' . $clientnum);
    echo '<table class="table table-condensed" id="matters-table">';
    echo '<tr>';
    echo '<th>Timekeeper</th>';
    echo '<th>Hours Worked</th>';
    echo '<th>Hours Billed</th>';
    foreach ($json_array as $row) {
        echo "<tr>";
        echo "<td><a href='people/" . $row->account . "'>" . $row->timekeeper . "</a></td>";
        echo "<td>" . $row->workhrs . "</td>";
        echo "<td>" . $row->billhrs . "</td>";
    }
    echo '</table>';
}

function get_matter_table($clientnum) {
    $json_array = getCURL_elite('getMatterTable/' . $clientnum);
    foreach ($json_array as $row) {
        echo '<table class="table table-condensed">';
        echo '<tr><td class="form-head">Client:</td>';
        echo '<td>' . $row->clname1 . ' [' . $row->clnum . ']</td></tr>';
        echo '<tr><td class="form-head">Address:</td>';
        echo '<td>' . $row->claddr1 . '<br/>' . $row->claddr2 . '<br/>' . $row->claddr3 . '</td></tr>';
        echo '<tr><td class="form-head">Phone:</td>';
        echo '<td>' . $row->clphone . '</td></tr>';
        echo '<tr><td class="form-head">Fax:</td>';
        echo '<td>' . $row->clfax . '</td></tr>';
        echo '<tr><td class="form-head">Status:</td><td>';
        if ($row->clstatus == 'C')
            echo 'Current';
        else
            echo 'Inactive';
        echo '</td></tr>';
        echo '<tr><td class="form-head">Open Date:</td>';
        echo '<td>' . date("m/d/Y", strtotime($row->clopendt->date)) . '</td>';
        echo '<tr><td class="form-head">Office:</td>';
        echo '<td>' . $row->office . '</td></tr>';
        echo '<tr><td class="form-head">Related Client:</td>';
        echo '<td><a href="client/' . $row->crelated . '">' . $row->crelatedname . '</a> [' . $row->crelated . ']</td></tr>';
        echo '</table>';
    }
}

function get_matter_overview($clientnum) {
    $json_array = getCURL_elite('getMatterOverview/' . $clientnum);
    foreach ($json_array as $row) {
        echo '<table class="table table-condensed">';
        echo '<tr><td class="form-head">Matter:</td>';
        echo '<td>' . $row->mname . '</td></tr>';
        echo '<tr><td class="form-head">Client:</td>';
        echo '<td>' . $row->cname . '</td></tr>';
        echo '<tr><td class="form-head">Address:</td>';
        echo '<td>' . $row->maddr1 . '<br/>' . $row->maddr2 . '<br/>' . $row->maddr3 . '</td></tr>';
        echo '<tr><td class="form-head">Phone:</td>';
        echo '<td>' . $row->mphone . '</td></tr>';
        echo '<tr><td class="form-head">Fax:</td>';
        echo '<td>' . $row->mfax . '</td></tr>';
        echo '<tr><td class="form-head">Status:</td><td>';
        if ($row->status == 'C')
            echo 'Current';
        else
            echo 'Inactive';
        echo '</td></tr>';
        echo '<tr><td class="form-head">Open Date:</td>';
        echo '<td>' . date("m/d/Y", strtotime($row->opendate->date)) . '</td>';
        echo '<tr><td class="form-head">Office:</td>';
        echo '<td>' . $row->office . '</td></tr>';
        echo '<tr><td class="form-head">Practice:</td>';
        echo '<td>' . $row->practice . '</td></tr>';
        echo '<tr><td class="form-head">Department:</td>';
        echo '<td>' . $row->department . '</td></tr>';
        echo '<tr><td class="form-head">Originating Attorney:</td>';
        echo '<td><a href="people/' . $row->orgattyaccount . '">' . $row->orgatty . '</a></td></tr>';
        echo '<tr><td class="form-head">Supervising Attorney:</td>';
        echo '<td><a href="people/' . $row->supattyaccount . '">' . $row->supatty . '</a></td></tr>';
        echo '<tr><td class="form-head">Billing Attorney:</td>';
        echo '<td><a href="people/' . $row->billattyaccount . '">' . $row->billatty . '</a></td></tr>';
        echo '<tr><td class="form-head">Related Matter:</td>';
        echo '<td><a href="matter?rmnum=' . $row->mnum . '&rname=' . $row->rname . '">' . $row->rname . '</a> [' . $row->rcmnum . ']</td></tr>';
        echo '<tr><td class="form-head">Refered By:</td>';
        echo '<td>' . $row->mrefer . '</td></tr>';
        echo '</table>';
    }
}

function get_recent_matters($clientnum) {
    $json_array = getCURL_elite('getRecentMatters/' . $clientnum);
    echo '<table class="table table-hover">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>Billing Attorney</th>';
    echo '<th>Matter Name</th>';
    echo '<th>Matter #</th>';
    echo '<th>Open Date</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    foreach ($json_array as $row) {
        echo "<tr>";
        echo "<td><a href='people/" . str_replace("@pepperlaw.com", "", $row->tkemail) . "'>" . $row->baname . "</a></td>";
        echo "<td><a href='matter?cmnum=" . $row->mmatter . "'>" . $row->mdesc1 . "</a></td>";
        echo "<td><a href='matter?cmnum=" . $row->mmatter . "'>" . $row->mnum . "</a></td>";
        echo "<td>" . date("m/d/Y", strtotime($row->mopendt->date)) . "</td>";
        echo "</tr>";
    }
    echo '</tbody></table>';
}

function get_matters($clientnum) {
    $json_array = getCURL_elite('getMatters/' . $clientnum);
    echo '<table class="table table-hover datatable">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>Matter</th>';
    echo '<th>Matter #</th>';
    echo '<th>Status</th>';
    echo '<th>Open Date</th>';
    echo '<th>Department</th>';
    echo '<th>Office</th>';
    echo '<th>Supervising Attorney</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';

    foreach ($json_array as $row) {
        echo "<tr>";
        echo "<td><a href='matter/" . $row->cmnum . "'>" . $row->mname . "</a></td>";
        echo "<td><a href='matter/" . $row->cmnum . "'>" . $row->mnum . "</a></td>";
        echo "<td>" . $row->status . "</td>";
        echo '<td>' . date("m/d/Y", strtotime($row->mopendt->date)) . '</td>';
        echo "<td>" . $row->department . "</td>";
        echo "<td>" . $row->office . "</td>";
        echo "<td><a href='people/" . str_replace("@pepperlaw.com", "", $row->supattyemail) . "'>" . $row->supatty . "</a></td>";
        echo "</tr>";
    }
    echo '</tbody></table>';
}

function new_matters_all($limit) {
    $json_array = getCURL_elite('newMattersAll/' . $limit);
    echo '<table class="table table-condensed" id="matters-table">';
    echo '<tr>';
    echo '<th>Client Name</th>';
    echo '<th>Client#</th>';
    echo '<th>Matter Name</th>';
    echo '<th>Matter#</th>';
    echo '<th>Billing Attorney</th>';
    echo '<th>Open Date</th></tr>';
    foreach ($json_array as $row) {
        echo "<tr>";
        echo "<td><a href='client/" . $row->cnum . "'>" . $row->cname . "</a></td>";
        echo "<td><a href='client/" . $row->cnum . "'>" . $row->cnum . "</a></td>";
        echo "<td>" . $row->mdesc1 . "</td>";
        echo "<td>" . $row->mnum . "</td>";
        echo "<td>" . $row->baname . "</td>";
        echo "<td>" . date("m/d/Y", strtotime($row->mopendt->date)) . "</td></tr>";
    }
    echo '</table>';
}

