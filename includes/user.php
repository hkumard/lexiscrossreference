<?php //require_once("config.php");


function getCURL($method)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,'http://pepperphp/firm-data-api/'.$method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);     
    return json_decode($output);
}
//
//function getCURL_elite($method)
//{
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL,'http://pepperphp/elite-api/'.$method);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    $output = curl_exec($ch);
//    curl_close($ch);     
//    return json_decode($output);
//}
function setUserInfo($username) {
    $json_array = getCURL("getAllUserInfo/".$username);
    if ($json_array!==null) {
        
        foreach($json_array as $row)
        {
            if ($row != null) {
                $_SESSION['username'] = $row->Username;
                $_SESSION['displayname'] = $row->DisplayName;
                $_SESSION['firstname'] = $row->FirstName;
                $_SESSION['lastname'] = $row->LastName;
                $_SESSION['office'] = $row->Office;
                $_SESSION['office_id'] = $row->PKOfficeID;
                $_SESSION['eoc'] = $row->EliteOfficeCode;
                $_SESSION['floor'] = $row->Floor;
                $_SESSION['department'] = $row->Department;
                $_SESSION['phone'] = $row->Phone;
                $_SESSION['tkid'] = $row->TimeKeeperID;

                $class_string = "";

                if ($row->PKOfficeID != null) {
                    $class_string = $row->PKOfficeID;
                } else { 
                    $class_string = "0";
                }

                if ($row->PracticeGroupID != null) {
                    $class_string .= "-" . $row->PracticeGroupID;
                } else {
                    $class_string .= "-0";
                }
                $_SESSION['userclass'] = $class_string;
            }
        }
    }
}

function checkEstatesUserAccess($tkid) {
    $ret = false;
    $_SESSION['admin'] = false;
    $db = Xcrud_db::get_instance();
    $select = "SELECT tkid,admin FROM user_access where tkid = " . $tkid;
    $db->query($select);
    $arr = $db->result();
    if (count($arr) > 0) {
        $ret = true;
        if ($arr[0]['admin']) {
            $_SESSION['admin'] = true;
        }
    }
    return $ret;
}