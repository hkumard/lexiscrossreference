<?php echo $this->render_table_name($mode); ?>
<div class="xcrud-top-actions">
    <?php echo $this->render_button('save_new','save','create','xcrud-button xcrud-blue','','create,edit') ?>
    <?php echo $this->render_button('save_edit','save','edit','xcrud-button xcrud-green','','create,edit') ?>
    <?php echo $this->render_button('save_return','save','list','xcrud-button xcrud-purple','','create,edit') ?>
    <?php echo $this->render_button('return','list','','xcrud-button xcrud-orange') ?>
</div>
<?php
$r=$this->fields_output;
$TotalScore1=$r['Matrix.Conflict']['value']+$r['Matrix.ClientRelationship']['value']+$r['Matrix.CompetitiveIntelligence']['value']+$r['Matrix.TimeToComplete']['value']+$r['Matrix.Requirements']['value']+$r['Matrix.Process']['value']+$r['Matrix.CompetitiveAdvantage']['value']+$r['Matrix.Qualifications']['value']+$r['Matrix.StrategicAdvantage']['value']+$r['Matrix.Consistency']['value']+$r['Matrix.ProjectTeamAvailability']['value']+$r['Matrix.PricingRequirements']['value']+$r['Matrix.ProfitPotential']['value']+$r['Matrix.CostToRespond']['value']+$r['Matrix.OddsOfWinning']['value'];
$OverallRating=$TotalScore1 / 15;
$goScore=15;


?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <style>
table td {
    border-top: none !important;
}

  </style>

<div class="container">
  <!--HTML generated from Microsoft Word template -->
  <table class=table borderless border=0 cellspacing=0 cellpadding=0 width=818
 style='width:613.6pt;margin-left:5.4pt;border-collapse:collapse'>
 <tr style='height:13.15pt'>
  <td width=37 nowrap valign=bottom style='width:28.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  
  <td width=124 nowrap valign=bottom style='width:122.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
             >CLIENT NAME: </span></b></p>
  </td>
  <td width=257 nowrap colspan=3 valign=bottom style='width:192.8pt;padding:
  0in 5.4pt 0in 5.4pt;height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
  ><?php echo $r['Matrix.ClientName']['field']; ?></span></p>
  </td>
 </tr>
 <tr style='height:13.15pt'>
  <td width=37 nowrap valign=bottom style='width:28.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
  >MATTER:</span></b></p>
  </td>
  <td width=185 nowrap colspan=2 valign=bottom style='width:138.95pt;
  padding:0in 5.4pt 0in 5.4pt;height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
  ><?php echo $r['Matrix.Matter']['field'];?></span></p>
  </td>

 </tr>
 <tr style='height:13.15pt'>
  <td width=37 nowrap valign=bottom style='width:28.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif"'>LEAD ATTORNEY: </span></b></p>
  </td>
  <td width=257 nowrap colspan=3 valign=bottom style='width:192.8pt;padding:
  0in 5.4pt 0in 5.4pt;height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
  ><?php echo $r['Matrix.LeadAttorney']['field']; ?></p>
  </td>
  
 </tr>
 <tr style='height:13.15pt'>
  <td width=37 nowrap valign=bottom style='width:28.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
>DUE DATE </span></b></p>
  </td>
  <td width=113 nowrap valign=bottom style='width:85.1pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial","sans-serif";'
><?php echo $r['Matrix.DueDate']['field']; ?></span></p>
  </td>
  
 </tr>
 <tr style='height:13.15pt'>
  <td width=37 nowrap valign=bottom style='width:28.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=124 nowrap valign=bottom style='width:92.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:10.0pt;font-family:"Arial","sans-serif";
  color:black'>RESULT: </span></b></p>
  </td>
  <td width=113 nowrap valign=bottom style='width:85.1pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:10.0pt;font-family:"Arial","sans-serif";
  color:black'><?php echo $r['Matrix.Result']['field']; ?></span></p>
  </td>
  <td width=72 nowrap valign=bottom style='width:53.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=76 nowrap valign=bottom style='width:56.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:13.15pt'></td>
 </tr>
 <tr style='height:15.75pt'>
  <td width=346 nowrap colspan=4 valign=bottom style='width:259.75pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'></td>
  <td width=72 nowrap valign=bottom style='width:53.85pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
  <td width=86 nowrap valign=bottom style='width:.9in;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
  <td width=76 nowrap valign=bottom style='width:56.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'></td>
 </tr>
 <tr style='height:46.5pt'>
  <td width=346 nowrap colspan=4 valign=bottom style='width:259.75pt;
  padding:0in 5.4pt 0in 5.4pt;height:46.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span style='position:absolute;z-index:251658240;margin-left:33px;
  margin-top:15px;width:216px;height:38px'><img width=216 height=38
             src="images/PepperLogo.jpg" alt=phlogo2color></span></p>
  <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0>
   <tr style='height:46.5pt'>

   </tr>
  </table>
  </td>
  <td width=407 nowrap colspan=5 style='width:305.05pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:46.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:14.0pt;
  font-family:"Arial Black","sans-serif";color:black'>RFP GO/NO-GO MATRIX</span></p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:46.5pt'></td>
 </tr>
 <tr style='height:12.75pt'>
  <td width=753 nowrap colspan=9 valign=bottom style='width:564.8pt;padding:0in 5.4pt 0in 5.4pt;height:12.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";color:black'>&nbsp;</span></p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:12.75pt'></td>
 </tr>
 <tr style='height:16.5pt'>
  <td width=161 colspan=2 rowspan=3 style='width:120.8pt;border:solid windowtext 1.0pt;padding: 0;
  border-top:1px;height:16.5pt'>
  <p align=center style='  border-top: solid windowtext 1.0pt;margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Proposal Factors</span></b></p>
  </td>
  <td width=516 colspan=6 style='width:387.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0;height:16.5pt'>
  <p align=center style='border-top: solid windowtext 1.0pt;margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Decision Criteria</span></b></p>
  </td>
  <td width=76 rowspan=3 style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0;height:16.5pt'>
  <p align=center style='border-top: solid windowtext 1.0pt;margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Rating</span></b></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:16.5pt'></td>
 </tr>
 <tr style='height:15.75pt'>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Negative</span></b></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Neutral</span></b></p>
  </td>
  <td width=173 nowrap colspan=2 valign=bottom style='width:1.8in;border-top:
  none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>Positive</span></b></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.75pt'></td>
 </tr>
 <tr style='height:15.75pt'>
  <td width=113 style='width:85.1pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>1</span></b></p>
  </td>
  <td width=72 style='width:53.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>2</span></b></p>
  </td>
  <td width=72 style='width:53.85pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>3</span></b></p>
  </td>
  <td width=86 style='width:.9in;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>4</span></b></p>
  </td>
  <td width=86 style='width:.9in;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>5</span></b></p>
  </td>
  <td width=86 style='width:.9in;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:15.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>6</span></b></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:15.75pt'></td>
 </tr>
 <tr style='height:27.0pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:27.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>1</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:27.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Conflict</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:27.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Conflict exists and is unlikely to be resolved*</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:27.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Conflict exists but it can be waived or resolved</span></p>
  </td>
  <td width=173 nowrap colspan=2 style='width:1.8in;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:27.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Clear of conflict</span></p>
  </td>
  <td width=76 style='width:56.8pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:27.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'><?php echo $r['Matrix.Conflict']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:27.0pt'></td>
 </tr>
 <tr style='height:23.25pt'>
  <td width=753 colspan=9 style='width:564.8pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:23.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>*If this factor is rated
  negatively, the RFP is automatically a “no-go” and the process ends at this
  stage.</span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:23.25pt'></td>
 </tr>
 <tr style='height:45.75pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>2</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Client Relationship</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Unknown or virtually unknown to client</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Known to client, but not well- known</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Is an existing client or a client/contact with whom the firm or
  an individual has a well-developed relationship </span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php print $r['Matrix.ClientRelationship']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:45.75pt'></td>
 </tr>
 <tr style='height:43.5pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>3</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Competitive Intelligence</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Did not expect RFP; only information we have is what’s included
  in the  solicitation</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Anticipated RFP, have collected adequate information to respond</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Anticipated RFP, have distinct insights into client needs and
  expectations </span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:43.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'><?php echo $r['Matrix.CompetitiveIntelligence']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:43.5pt'></td>
 </tr>
 <tr style='height:69.75pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>4</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Time to Complete</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Deadline is too short and is inflexible – i.e., less than 24-48
  hours; does not account for a U.S. national holiday; or is too short relative
  to the RFP requirements</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Adequate time is given to complete the RFP (i.e. one to two
  weeks)</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Ample time is given to complete the RFP response  (i.e. three or
  more weeks)</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:69.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.TimeToComplete']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:69.75pt'></td>
 </tr>
 <tr style='height:57.75pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>5</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Requirements</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span'>Includes inflexible terms/conditions or contracts, requires
  confidential information to be disclosed, etc. </span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Includes some requirements that may be considered burdensome,
  but is willing to negotiate or demonstrates flexibility </span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>No burdensome requirements are included in the RFP</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:57.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.Requirements']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:57.75pt'></td>
 </tr>
 <tr style='height:84.75pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>6</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Process</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>No Q&amp;A process or issuer declines to provide sufficient
  information to respond (i.e., demands specific fixed fee but will not provide
  data needed to quote a price); or requests excessive data having no relevance
  to the work</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Q&amp;A process is included; answers are reasonably responsive</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Issuer engages in detailed information exchange and answers all
  questions throughout RFP process</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.Process']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:84.75pt'></td>
 </tr>
 <tr style='height:60.0pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>7</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Competitive Advantage</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Competitor is strongly favored; client is happy with current
  counsel relationship (may have issued RFP to drive down/leverage pricing)</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Open competition with no apparent favorite; possibly a
  convergence RFP or a shift to a panel/preferred counsel relationship</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Pepper is in a favored position for contract award</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:60.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.CompetitiveAdvantage']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:60.0pt'></td>
 </tr>
 <tr style='height:84.0pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>8</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Qualifications and Experience</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Limited or no relevant experience in the practice in question,
  or client’s needs are not a good match for  Pepper’s experience; have not
  worked with similar clients in the past</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Adequately qualified but no real edge over competitors; have
  worked with some similar clients in the past</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Technically superior to most competitors; experience is an exact
  match with the client’s request in terms of legal work and/or industry
  experience; have worked with many similarly situated clients</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:84.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.Qualifications']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:84.0pt'></td>
 </tr>
 <tr style='height:45.75pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>9</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Strategic Advantage</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span'>Client’s goal or the path to a successful conclusion of the
  matter is unclear</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Path to success is relatively clear based on experience; need
  more detail from the client to finalize approach</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Case/matter strategy – and how Pepper can position the client
  for success – is clear</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:45.75pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'><?php echo $r['Matrix.StrategicAdvantage']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:45.75pt'></td>
 </tr>
 <tr style='height:40.5pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>10</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Consistency with Goals</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Opportunity not consistent with practice group’s strategic
  targets</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Opportunity fits within stated practice group goals for the
  types of clients/work </span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Opportunity can’t be passed up relative to strategic targets</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:40.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.Consistency']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:40.5pt'></td>
 </tr>
 <tr style='height:59.25pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>11</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Project Team Availability</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Needed team members are too busy to help with the RFP response
  or to do the work if we are awarded the business</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Needed team members have adequate availability to assist with
  the RFP and to staff matters if we are awarded the business</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Very strong proposed team with good availability to work on the
  RFP and to staff matters if we are awarded the business</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:59.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.ProjectTeamAvailability']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:59.25pt'></td>
 </tr>
 <tr style='height:39.0pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>12</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Pricing Requirements</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Requires a steep discount as a point of entry or a multi-year
  rate freeze</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Has a balanced mix of favorable/unfavorable requirements; open
  to negotiation </span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Flexible/open to a variety of AFAs</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:39.0pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'><?php echo $r['Matrix.PricingRequirements']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:39.0pt'></td>
 </tr>
 <tr style='height:28.5pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:9.0pt;
  ;color:black'>13</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span 
  color:black'>Pricing Sensitivity</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Selection primarily driven by price; commodity purchase</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Client to balance price and qualifications in selection</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Will select most qualified, then negotiate price, or experience </span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.PricingSensitivity']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'></td>
 </tr>
 <tr style='height:32.25pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>14</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Profit Potential</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Unlikely to make a profit on this project</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Can meet profit goals if matters are well-managed</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>High likelihood to meet or exceed targeted profit</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:32.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.ProfitPotential']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:32.25pt'></td>
 </tr>
 <tr style='height:28.5pt'>
  <td width=37 style='width:28.0pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>15</span></p>
  </td>
  <td width=124 style='width:92.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Cost to Respond</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>High proposal costs relative to odds of winning</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Proposal costs appropriate relative to odds</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Cost is very appropriate for the odds</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:28.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.CostToRespond']['field']; ?></span></p>
  </td>
  <td width=65 style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;height:28.5pt'></td>
 </tr>
 <tr style='height:22.5pt'>
  <td width=37 style='width:28.0pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:22.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span>16</span></p>
  </td>
  <td width=124 style='width:92.8pt;border:none;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.5pt;border-bottom:
  solid windowtext 1.0pt;'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>Odds of Winning</span></p>
  </td>
  <td width=185 colspan=2 style='width:138.95pt;border:none;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.5pt;border-bottom:
  solid windowtext 1.0pt;'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>0-30% chance of winning</span></p>
  </td>
  <td width=158 colspan=2 style='width:118.65pt;border:none;border-right:solid windowtext 1.0pt;border-bottom:
  solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>30-60% chance of winning</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border:none;border-right:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.5pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><span>60-90% chance of winning</span></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border:none;border-right:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:22.5pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span><?php echo $r['Matrix.OddsOfWinning']['field']; ?></span></p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:22.5pt'></td>
 </tr>
 <tr style='height:17.25pt'>
  <td width=505 colspan=6 style='width:378.4pt;border-top:solid windowtext 1.0pt;
  border-left:solid windowtext 1.5pt;border-bottom:solid windowtext 1.0pt;
  border-right:none;padding:0in 5.4pt 0in 5.4pt;height:17.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>Total Score</span></b><span style='font-size:9.0pt;font-family:
  "Arial Narrow","sans-serif";color:black'> (sum of 15 proposal factor ratings)</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:solid windowtext 1.0pt;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:17.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>&nbsp;</span></b></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:17.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><span style='font-size:10.0pt;
  font-family:"Arial","sans-serif";color:black'><?php echo $r['Matrix.TotalScore']['value']; ?></span></p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:17.25pt'></td>
 </tr>
 <tr style='height:.25in'>
  <td width=505 colspan=6 style='width:378.4pt;border-top:none;border-left:
  solid windowtext 1.5pt;border-bottom:solid windowtext 1.0pt;border-right:
  none;padding:0in 5.4pt 0in 5.4pt;height:.25in'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>Overall Rating </span></b><span style='font-size:9.0pt;
  ;color:black'>(total score divided by
  15)</span></p>
  </td>
  <td width=173 colspan=2 style='width:1.8in;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:.25in'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>&nbsp;</span></b></p>
  </td>
  <td width=76 nowrap style='width:56.8pt;border:solid windowtext 1.0pt;
  border-left:none;padding:0in 5.4pt 0in 5.4pt;height:17.25pt'>
  <p align=center style='font-size:10.0pt;
  font-family:"Arial","sans-serif";color:black'><span><?php echo $r['Matrix.OverallRating']['value']; ?></span></p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:.25in'></td>
 </tr>
 <tr style='height:77.25pt'>
  <td width=505 colspan=6 valign=top style='width:378.4pt;border-top:none;
  border-left:solid windowtext 1.5pt;border-bottom:solid windowtext 1.0pt;
  border-right:none;padding:0in 5.4pt 0in 5.4pt;height:77.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>COMMENTS:  </span></b><span style='font-size:9.0pt;font-family:
  "Arial Narrow","sans-serif";color:black'><?php echo $r['Matrix.Comments']['field']; ?></span></p>
  </td>
  <td width=173 colspan=2 valign=top style='width:1.8in;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:77.25pt'>
  <p style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
  normal'><b><span>&nbsp;</span></b></p>
  </td>
  <td width=76 style='width:56.8pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;
  height:77.25pt'>
  <p align=center style='margin-bottom:0in;margin-bottom:.0001pt;
  text-align:center'><b><span style='font-size:9.0pt;
  ;color:black'>DECISION:<br>
  </span></b><b><span style='font-size:15.0pt;;
  color:black'><?php echo $r['Matrix.Status']['value']; ?> </span></b>
      <b><span 
  style='color:#00B050'></span></b>
</p>
  </td>
  <td width=65 nowrap valign=bottom style='width:48.8pt;padding:0in 5.4pt 0in 5.4pt;
  height:77.25pt'></td>
 </tr>
</table>

<div class="xcrud-bottom-actions text-center">
<?php echo $this->render_button('save_return','save','list','btn btn-success','fa fa-floppy-o','create,edit') ?>
<?php echo $this->render_button('cancel','list','','btn btn-warning','fa fa-reply') ?>
</div>

