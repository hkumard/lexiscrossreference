/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#FF0000';
    config.toolbar = [
		['Source'],
      ['Cut','Copy','Paste','PasteText','PasteFromWord','SpellChecker'],
      ['Undo','Redo'],
      ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
      ['NumberedList','BulletedList','-','Blockquote'],
      ['Link','Unlink'], 
	]
};

