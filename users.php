<?php require_once "header.php"; ?>
<a href="search.php" class="btn btn-default">&laquo; Back to Search</a>
<?php
$field = $_GET['f'];
$value = $_GET['v'];

if ($field === 'tkinit') {
    $xcrud = Xcrud::get_instance();
    $xcrud->table('timekeep');
    $xcrud->where($field, $value);
    $xcrud->unset_title();
    $xcrud->columns('tksort,tktitle,tktmdate,tkloc,tkemdate,tkinitb,tglmask,tkblast,tkbfirst,tkstrate,tkdept,tksect,'
            . 'tkschool,tkgrdate,tkbrdate,tkstdcst,tprotem,tkeflag,tkemail,tkmoddate,tkmodtime,tkmoduser,tkceflag,unit,tkdegree,tkphone,'
            . 'tksectretary1,tksectretary2', true);
    $xcrud->fields('tkinit,tklast,tkfirst', false, 'Main Form');
    $xcrud->button('lexis.php?t={tkinit}', 'Edit Lexis Records', 'glyphicon glyphicon-pencil');
    $xcrud->set_attr('tkinit', array('id' => 'tkinit'));
    $xcrud->set_attr('tklast', array('id' => 'tklast'));
    $xcrud->set_attr('tkfirst', array('id' => 'tkfirst'));
    $xcrud->label('tkinit', 'TimekeeperID');
    $xcrud->label('tklast', 'Last Name');
    $xcrud->label('tkfirst', 'First Name');
    $xcrud->unset_edit();
    $xcrud->unset_search();
    $xcrud->unset_print();
    $xcrud->unset_csv();
    $xcrud->unset_add();
    $xcrud->unset_view();
    $xcrud->unset_remove();


    echo $xcrud->render();
} else if ($field ==='tklast' ) {
    $xcrud = Xcrud::get_instance();
    $ret1 = explode(',', $value);
    $abc1 = $ret1[0];
    $value = $abc1;
    $xcrud->table('timekeep');
    $xcrud->where($field, $value);
    $xcrud->unset_title();
    $xcrud->columns('tksort,tktitle,tktmdate,tkloc,tkemdate,tkinitb,tglmask,tkblast,tkbfirst,tkstrate,tkdept,tksect,'
            . 'tkschool,tkgrdate,tkbrdate,tkstdcst,tprotem,tkeflag,tkemail,tkmoddate,tkmodtime,tkmoduser,tkceflag,unit,tkdegree,tkphone,'
            . 'tksectretary1,tksectretary2', true);
    $xcrud->fields('tkinit,tklast,tkfirst', false, 'Main Form');
    $xcrud->button('lexis.php?t={tkinit}', 'Edit Lexis Records', 'glyphicon glyphicon-pencil');
    $xcrud->set_attr('tkinit', array('id' => 'tkinit'));
    $xcrud->set_attr('tklast', array('id' => 'tklast'));
    $xcrud->set_attr('tkfirst', array('id' => 'tkfirst'));
    $xcrud->label('tkinit', 'TimekeeperID');
    $xcrud->label('tklast', 'Last Name');
    $xcrud->label('tkfirst', 'First Name');
    $xcrud->unset_edit();
    $xcrud->unset_search();
    $xcrud->unset_print();
    $xcrud->unset_csv();
    $xcrud->unset_add();
    $xcrud->unset_view();
    $xcrud->unset_remove();
    echo $xcrud->render();
}

else if ($field ==='tkfirst' ) {
    $xcrud = Xcrud::get_instance();
    $ret1 = explode(',', $value);
    $abc1 = $ret1[0];
    
    $value = $abc1;
    $xcrud->table('timekeep');
    $xcrud->where($field, $value);
    $xcrud->unset_title();
    $xcrud->columns('tksort,tktitle,tktmdate,tkloc,tkemdate,tkinitb,tglmask,tkblast,tkbfirst,tkstrate,tkdept,tksect,'
            . 'tkschool,tkgrdate,tkbrdate,tkstdcst,tprotem,tkeflag,tkemail,tkmoddate,tkmodtime,tkmoduser,tkceflag,unit,tkdegree,tkphone,'
            . 'tksectretary1,tksectretary2', true);
    $xcrud->fields('tkinit,tklast,tkfirst', false, 'Main Form');
    $xcrud->button('lexis.php?t={tkinit}', 'Edit Lexis Records', 'glyphicon glyphicon-pencil');
    $xcrud->set_attr('tkinit', array('id' => 'tkinit'));
    $xcrud->set_attr('tklast', array('id' => 'tklast'));
    $xcrud->set_attr('tkfirst', array('id' => 'tkfirst'));
    $xcrud->label('tkinit', 'TimekeeperID');
    $xcrud->label('tklast', 'Last Name');
    $xcrud->label('tkfirst', 'First Name');
    $xcrud->unset_edit();
    $xcrud->unset_search();
    $xcrud->unset_print();
    $xcrud->unset_csv();
    $xcrud->unset_add();
    $xcrud->unset_view();
    $xcrud->unset_remove();
    echo $xcrud->render();
}
?>
<?php require_once "footer.php"; ?>