///* 
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//$('a').on("click", function(){
//   var linkAddress = $(this).attr('href');
//   if (linkAddress.charAt(0) != '#') {
//       var ajaxOpts = {  
//            type: "POST",
//            url: "api/track-click.php?id="+linkAddress,
//        };
//        $.ajax(ajaxOpts);
//   }
//   return true;
//});

function checkNull(variable) {
    if (variable !== null && variable !== undefined)
        return variable;
    else
        return '';
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    //alert (vars);
    return vars;
    
}

function HSQ(item) {
    return item.replace(/\'/g, "\''");
}

function SSQ(item) {
    return item.replace(/\''/g, "\'");
}

function space_to_dash(s) {
    str = s.replace(/\s+/g, '-');
    return str;
}

function dash_to_space(s) {
    str =  s.replace(/-/g, ' ');
    return str;
}

//CKEditor Code for locking, unlocking and updating Static Content Editors
function displayStatus(type, msg, fadeout) {
    if (type=="danger") {
        document.write(msg);
    } else {
        $("#divStatus").addClass("alert-"+type);
        $("#divStatus").toggleClass('in');
        $("#divStatus span").text(msg);
        setTimeout(function() {
            $('#divStatus').removeClass('in');
        }, 3000);
    }
    
}

function displayError(type, msg, fadeout) {
    //document.write(msg);
    /*$("#divStatus").addClass("alert-"+type);
    $("#divStatus").toggleClass('in');
    $("#divStatus span").text(msg);*/
}

$("#btn_lock").click(function() {
    $(this).toggleClass('btn-danger');
    if ($(this).text().toLowerCase().trim() === 'unlock for editing') {
        $(this).text('edit mode');
        enable_CK();
    }else{
        $(this).text('unlock for editing');
        disable_CK();
    }
});

function enable_CK() {
    $(".block").each(function(){
        $(this).toggleClass('block-border');
        
        $(this).attr('contenteditable', true);
        var content_id = $(this).attr('id');
        
        CKEDITOR.inline( content_id, {
            on: {
                blur: function( event ) {
                    item_id = content_id.substr(3);
                    var data = event.editor.getData();
                    params = {};
                    params["content_text"] = data;
                    var ajaxOpts = {  
                        type: "POST",
                        url: "api/saveStaticHTML/"+item_id,
                        data: params,
                        async: false,
                        success: function(data){
                             displayStatus("success", "Page Updated", 5000);
                        }
                    };
                    $.ajax(ajaxOpts);
                }
            }
        } );
    });
}

function disable_CK() {
   $(".block").each(function(){
        $(this).toggleClass('block-border');
        $(this).attr('contenteditable', false);
        var content_id = $(this).attr('id');
        CKEDITOR.instances[ content_id ].destroy();
   });
}

function showhideAdmin(admin) {
    //check if user is Admin on page, if so hide
    if (admin === 'admin') {
        $('.btn-admin').show();
    } else {
        $('.btn-admin').hide();
    }
}

function showNewHires() {
    $("#new-hires li").hide();
 
    var headlineFadeSpeed = 10000;
    function showNextItem(){
        var index      = arguments[0] || 1;
        var $curLi     = $("#new-hires li:nth-child("+index+")");
        var totalItems = $("#new-hires li").length;
 
        $curLi.fadeIn("fast", function(){
            setTimeout(function(){
                
                $curLi.fadeOut("slow", function(){
                    index = (index < totalItems) ? index+1 : 1;
                    showNextItem(index);
                });
            }, headlineFadeSpeed);
        });
    }
    showNextItem();
}

function load_calendar(c) {
    var url="api/get-calendar-items.php?c="+c;
    $('#cal_div').fullCalendar({
        editable: false,
        events: url,
        allDayDefault:false,
        eventRender: function (event, element) {
            element.attr('href', 'javascript:void(0);');
            element.attr('onclick', 'loadCalModal(' + event.id + ');');
        },
        loading: function(bool) {
            if (bool) $('#loading').show();
            else $('#loading').hide();
        }
    });
}

function fixFullCalinTab() {
    // Fix for fullCalendar to work in Tab
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#cal_div').fullCalendar('render');
    });
    //$('#pepper-tab a:first').tab('show');
}


$(function(){
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');
  $('.nav-tabs a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });
});

function bindDataTable(meth) {
    $('#example').DataTable( {
        buttons: true,
        "ajax": {
            "url": "http://pepperphp/firm-data-api/"+meth,
            "dataSrc": ""
        },
        "columns": [
            { "data": "Username",
                "render": function ( data, type, full, meta ) {
                    return '<a href="people/'+data+'"><img src="firm-directory-photos/' + data + '.jpg" class="img-rounded" style="width:75px" onerror="imgError(this);"/></a>';
                }
            },
            { "data": "Username",
                "render": function ( data, type, full, meta ) {
                    return '<a href="people/'+data+'">' + full.DisplayName + '</a>';
                } 
            },
            { "data": "Position" },
            { "data": "Username",
                "render": function ( data, type, full, meta ) {
                    return "<a href='mailto:" + full.Username + "@pepperlaw.com' title='Email " + full.DisplayName + "'><span class='glyphicon glyphicon-envelope med-glyph'></span></a>";
                } 
            },
            { "data": "Extension" },
            { "data": "Office" }
        ]
    } );
}

function imgError(image) {
    image.onerror = "";
    image.src = "assets/img/user-icon.png";
    return true;
}


$('.modal').css(
{
    'margin-top': function () {
        return -($(this).height() / 2)-100;
    }
})

