<?php

require_once("rest.inc.php");
require_once("../includes/config.php");

function utf_clean($item) {
    if (is_string($item)) {
        return(utf8_encode($item));
    } else {
        return $item;
    }
}

class API extends REST {

    public $data = "";
    private $db = NULL;

    public function __construct() {
        parent::__construct();    // Init parent contructor
        $this->dbConnect();     // Initiate Database connection
    }

    /* DATABASE CONNECTION */

    private function dbConnect() {

        $conninfo = array("UID" => DB_USER, "PWD" => DB_PASS, "Database" => DB_NAME);
        $this->db = sqlsrv_connect(DB_SERVER, $conninfo);
        if (!$this->db) {
            $this->response('Unable to connect to DB', 400);
        }
    }

    /* Public method for access api. This method dynmically call the method based on the query string  */

    public function processApi() {
        $rquest = $_REQUEST['rquest'];
        if (strrpos($_REQUEST['rquest'], "/")) {
            $arrReq = array();
            $arrReq = explode("/", $_REQUEST['rquest']);
            if (count($arrReq) > 1) {
                $param = $arrReq[1];
            }
            if (count($arrReq) > 0) {
                $func = $arrReq[0];
            }
        } else {
            $func = $_REQUEST['rquest'];
        }

        if ((int) method_exists($this, $func) > 0) {
            if (isset($param)) {
                $this->$func($param);
            } else {
                $this->$func();
            }
        } else {
            $this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
        }
    }

    // FUNCTION FOR RETURNING JSON FROM SQL STATEMENT
    private function getJSONFromSQL($sql = '') {
        if ($this->get_request_method() != "GET") {
            $this->response('', 406);
        }
        $stmt = sqlsrv_query($this->db, $sql);
        if (!$stmt) {
            $this->response($this->json('Unable to retrieve data - ' . $this->json(sqlsrv_errors()), 404));
        }
        $result = array();
        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
            if ($row == null) {
                $result[] = "";
            } else {
                $result[] = array_map('utf_clean', $row);
            }
        }
        //echo($this->json($result));
        $this->response($this->json($result), 200);
    }

    //	Encode array into JSON
    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

    private function pepperTKIDs2() {
        $sql = "SELECT tkinit as id, tklast+','+tkfirst  as label  FROM timekeep WHERE   tklast+','+tkfirst like '%" . addslashes($_GET['term']) . "%'"."order by  tklast+','+tkfirst ";
        $this->getJSONFromSQL($sql);
    }

    
        private function pepperTKIDs3() {
        $sql = "SELECT tkinit as id, tkfirst+','+tklast   as label  FROM timekeep WHERE   tklast+','+tkfirst like '%" . addslashes($_GET['term']) . "%'"."order by  tklast+','+tkfirst ";
        $this->getJSONFromSQL($sql);
    }

 }

// Initiiate Library

$api = new API;
$api->processApi();
?>