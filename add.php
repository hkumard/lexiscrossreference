<?php require_once "header.php"; ?>


<?php
$xcrud = Xcrud::get_instance();
$xcrud->table('ph_lexref');
//$xcrud->change_type('tkinit','select','',xcrudDropDownOptions('pepperUsers','TimekeeperID','DisplayName',true));

$xcrud->set_attr('userid', array('id' => 'userid'));

$xcrud->label('userid', 'Lexis ID');
$xcrud->column_callback('tkinit', 'gettimekeeper1');

$xcrud->change_type('tkinit', 'select', '', xcrudDropDownOptions('lexiscrossref', 'tkinit', 'timekeeper', true));

$xcrud->label('tkinit', 'TimeKeeper');
$xcrud->unset_title();
echo $xcrud->render('create');
//echo $xcrud->render();
?>
<?php require_once "footer.php"; ?>

<script>

    jQuery(document).on("ready xcrudafterrequest", function () {

        $("#userid").keyup(function () {
            this.value = this.value.toLocaleUpperCase();
        });

        if (Xcrud.current_task == 'save')
        {
            $(".xcrud").hide();
            window.location = "search.php";

        }
else {
            // console.log(Xcrud.current_task);
            $(".xcrud").show();
        }

 if (Xcrud.current_task == 'list')
        {
            $(".xcrud").hide();
            window.location = "search.php";

        }



    });
</script>